<!DOCTYPE html>
<html lang="en" >
    <!--begin::Head-->
        <head>
            <meta charset="utf-8"/>
            <title>Sign Up | MAM Assistive</title>
            <meta name="description" content="Singin page example"/>
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

            <!--begin::Fonts-->
                <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>        <!--end::Fonts-->


            <!--begin::Page Custom Styles(used by this page)-->
                <link href="{{ asset('css/pages/login/login-4.css') }}" rel="stylesheet" type="text/css"/>
            <!--end::Page Custom Styles-->

            <!--begin::Global Theme Styles(used by all pages)-->
                <link href="{{ asset('plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css"/>
                <link href="{{ asset('plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css"/>
                <link href="{{ asset('css/style.bundle.css') }}" rel="stylesheet" type="text/css"/>
            <!--end::Global Theme Styles-->

            <link rel="shortcut icon" href="{{ asset('media/logos/favicon.png') }}"/>

        </head>
    <!--end::Head-->

    <!--begin::Body-->
        <body  id="kt_body"  class="quick-panel-right demo-panel-right offcanvas-right header-fixed header-mobile-fixed subheader-enabled aside-enabled aside-static page-loading"  >

            <!--begin::Main-->
                <div class="d-flex flex-column flex-root">
                    <!--begin::Login-->
                        <div class="login login-4 wizard d-flex flex-column flex-lg-row flex-column-fluid wizard" id="kt_login">
                            <!--begin::Content-->
                                <div class="login-container d-flex flex-center flex-row flex-row-fluid order-2 order-lg-1 flex-row-fluid bg-white py-lg-0 pb-lg-0 pt-15 pb-12">
                                    <!--begin::Container-->
                                        <div class="login-content login-content-signup d-flex flex-column">
                                            <!--begin::Aside Top-->
                                                <div class="d-flex flex-column-auto flex-column px-10">
                                                    <!--begin::Aside header-->
                                                    <a href="#" class="login-logo pb-lg-4 pb-10">
                                                        <img src="{{ asset('media/logos/logo-4.png') }}" class="max-h-70px" alt=""/>
                                                    </a>
                                                    <!--end::Aside header-->

                                                    <!--begin: Wizard Nav-->
                                                        <div class="wizard-nav pt-5 pt-lg-15 pb-10">
                                                            <!--begin::Wizard Steps-->
                                                                <div class="wizard-steps d-flex flex-column flex-sm-row">
                                                                    <!--begin::Wizard Step 1 Nav-->
                                                                        <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step" data-wizard-state="current">
                                                                            <div class="wizard-wrapper pr-7">
                                                                                <div class="wizard-icon">
                                                                                    <i class="wizard-check ki ki-check"></i>
                                                                                    <span class="wizard-number">1</span>
                                                                                </div>
                                                                                <div class="wizard-label">
                                                                                    <h3 class="wizard-title">
                                                                                        Profile
                                                                                    </h3>
                                                                                    <div class="wizard-desc">
                                                                                        Account details
                                                                                    </div>
                                                                                </div>
                                                                                <span class="svg-icon pl-6">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                                            <polygon points="0 0 24 0 24 24 0 24"/>
                                                                                            <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1"/>
                                                                                            <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "/>
                                                                                        </g>
                                                                                    </svg>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    <!--end::Wizard Step 1 Nav-->

                                                                    <!--begin::Wizard Step 2 Nav-->
                                                                        <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step">
                                                                            <div class="wizard-wrapper pr-7">
                                                                                <div class="wizard-icon">
                                                                                    <i class="wizard-check ki ki-check"></i>
                                                                                    <span class="wizard-number">2</span>
                                                                                </div>
                                                                                <div class="wizard-label">
                                                                                    <h3 class="wizard-title">
                                                                                        Affiliation
                                                                                    </h3>
                                                                                    <div class="wizard-desc">
                                                                                        Where you belong
                                                                                    </div>
                                                                                </div>
                                                                                <span class="svg-icon pl-6">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                                            <polygon points="0 0 24 0 24 24 0 24"/>
                                                                                            <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1"/>
                                                                                            <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "/>
                                                                                        </g>
                                                                                    </svg>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    <!--end::Wizard Step 2 Nav-->

                                                                    <!--begin::Wizard Step 3 Nav-->
                                                                        <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step">
                                                                            <div class="wizard-wrapper">
                                                                                <div class="wizard-icon">
                                                                                    <i class="wizard-check ki ki-check"></i>
                                                                                    <span class="wizard-number">3</span>
                                                                                </div>
                                                                                <div class="wizard-label">
                                                                                    <h3 class="wizard-title">
                                                                                        Confirmation
                                                                                    </h3>
                                                                                    <div class="wizard-desc">
                                                                                        Submit form
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    <!--end::Wizard Step 3 Nav-->
                                                                </div>
                                                            <!--end::Wizard Steps-->
                                                        </div>
                                                    <!--end: Wizard Nav-->
                                                </div>
                                            <!--end::Aside Top-->

                                            <!--begin::Signin-->
                                                <div class="login-form">
                                                    <!--begin::Form-->
                                                    <form action="/register" method="post" class="form px-10 register_form" novalidate="novalidate" id="kt_login_signup_form">
                                                        {{ csrf_field() }}
                                                        <!--begin: Wizard Step 1-->
                                                            <div class=" " data-wizard-type="step-content" data-wizard-state="current">
                                                                <!--begin::Title-->
                                                                <div class="pb-10 pb-lg-12">
                                                                    <h3 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Create Account</h3>
                                                                    <div class="text-muted font-weight-bold font-size-h4">
                                                                        Already have an Account ?
                                                                        <a href="custom/pages/login/login-4/signin.html" class="text-primary font-weight-bolder">Sign In</a>
                                                                    </div>
                                                                </div>
                                                                <!--begin::Title-->

                                                                <!--begin::Form Group-->
                                                                <div class="form-group">
                                                                    <label class="font-size-h6 font-weight-bolder text-dark">First Name</label>
                                                                    <input type="text" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6" name="fname" placeholder="First Name" value="" />
                                                                </div>
                                                                <!--end::Form Group-->

                                                                <!--begin::Form Group-->
                                                                <div class="form-group">
                                                                    <label class="font-size-h6 font-weight-bolder text-dark">Last Name</label>
                                                                    <input type="text" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6" name="lname" placeholder="Last Name" value="" />
                                                                </div>
                                                                <!--end::Form Group-->

                                                                <!--begin::Form Group-->
                                                                <div class="form-group">
                                                                    <label class="font-size-h6 font-weight-bolder text-dark">Middle Name</label>
                                                                    <input type="text" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6" name="mname" placeholder="Middle Name" value="" />
                                                                </div>
                                                                <!--end::Form Group-->
                                                            </div>
                                                        <!--end: Wizard Step 1-->

                                                        <!--begin: Wizard Step 2-->
                                                            <div class="pb-5" data-wizard-type="step-content">
                                                                <!--begin::Title-->
                                                                <div class="pt-lg-0 pt-5 pb-15">
                                                                    <h3 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Affiliation Details</h3>
                                                                    {{-- <div class="text-muted font-weight-bold font-size-h4">
                                                                        Have a Different Address ?
                                                                        <a href="#" class="text-primary font-weight-bolder">Add Address</a>
                                                                    </div> --}}
                                                                </div>
                                                                <!--begin::Title-->

                                                                <!--begin::Row-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <label class="font-size-h6 font-weight-bolder text-dark">Affiliated School</label>
                                                                            <input type="text" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6" name="address1" placeholder="Address Line 1" value="Address Line 1" />
                                                                            <span class="form-text text-muted">Please enter school name.</span>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <label class="font-size-h6 font-weight-bolder text-dark">School Address</label>
                                                                            <input type="text" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6" name="address2" placeholder="Address Line 2" value="Address Line 2" />
                                                                            <span class="form-text text-muted">Please enter your Address.</span>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Row-->

                                                                <!--begin::Row-->
                                                                <div class="row">
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Input-->
                                                                        <div class="form-group">
                                                                            <label class="font-size-h6 font-weight-bolder text-dark">School Head</label>
                                                                            <input type="text" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6" name="state" placeholder="State" value="VIC" />
                                                                            <span class="form-text text-muted">Please enter name.</span>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                    <div class="col-xl-6">
                                                                        <!--begin::Select-->
                                                                        <div class="form-group">
                                                                            <label class="font-size-h6 font-weight-bolder text-dark">Department Head</label>
                                                                            <input type="text" class="form-control form-control-solid h-auto py-7 px-6 border-0 rounded-lg font-size-h6" name="state" placeholder="State" value="VIC" />
                                                                            <span class="form-text text-muted">Please enter name.</span>
                                                                        </div>
                                                                        <!--end::Input-->
                                                                    </div>
                                                                </div>
                                                                <!--end::Row-->
                                                            </div>
                                                        <!--end: Wizard Step 2-->

                                                        <!--begin: Wizard Step 3-->
                                                            <div class="pb-5" data-wizard-type="step-content">
                                                                <!--begin::Title-->
                                                                <div class="pt-lg-0 pt-5 pb-15">
                                                                    <h3 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Complete</h3>
                                                                    <div class="text-muted font-weight-bold font-size-h4">
                                                                        Complete Your Signup And Become A Member!
                                                                    </div>
                                                                </div>
                                                                <!--end::Title-->

                                                                <!--begin::Section-->
                                                                <h4 class="font-weight-bolder mb-3">
                                                                    Accoun Settings:
                                                                </h4>
                                                                <div class="text-dark-50 font-weight-bold line-height-lg mb-8">
                                                                    <div>Nick Stone</div>
                                                                    <div>+12233434-34</div>
                                                                    <div>nick.stone@gmail.com</div>
                                                                </div>
                                                                <!--end::Section-->

                                                                <!--begin::Section-->
                                                                <h4 class="font-weight-bolder mb-3">
                                                                    Address Details:
                                                                </h4>
                                                                <div class="text-dark-50 font-weight-bold line-height-lg mb-8">
                                                                    <div>Address Line 1</div>
                                                                    <div>Address Line 2</div>
                                                                    <div>Melbourne 3000, VIC, Australia</div>
                                                                </div>
                                                                <!--end::Section-->

                                                                <!--begin::Section-->
                                                                <h4 class="font-weight-bolder mb-3">
                                                                    Support Channels:
                                                                </h4>
                                                                <div class="text-dark-50 font-weight-bold line-height-lg mb-8">
                                                                    <div>Overnight Delivery with Regular Packaging</div>
                                                                    <div>Preferred Morning (8:00AM - 11:00AM) Delivery</div>
                                                                </div>
                                                                <!--end::Section-->
                                                            </div>
                                                        <!--end: Wizard Step 5-->

                                                        <!--begin: Wizard Actions-->
                                                            <div class="d-flex justify-content-between pt-7">
                                                                <div class="mr-2">
                                                                    <button type="button" class="btn btn-light-primary font-weight-bolder font-size-h6 pr-8 pl-6 py-4 my-3 mr-3" data-wizard-type="action-prev">
                                                                        <span class="svg-icon svg-icon-md mr-2"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Left-2.svg-->
                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>
                                                                                    <rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000) " x="14" y="7" width="2" height="10" rx="1"/>
                                                                                    <path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997) "/>
                                                                                </g>
                                                                            </svg><!--end::Svg Icon-->
                                                                        </span>
                                                                        Previous
                                                                    </button>
                                                                </div>
                                                                <div>
                                                                    <button type="button" class="btn btn-primary font-weight-bolder font-size-h6 pl-8 pr-4 py-4 my-3 registerbutton" data-wizard-type="action-submit" type="submit" id="kt_login_signup_form_submit_button">
                                                                        Submit
                                                                        <span class="svg-icon svg-icon-md ml-2"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Right-2.svg-->
                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>
                                                                                    <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1"/>
                                                                                    <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "/>
                                                                                </g>
                                                                            </svg><!--end::Svg Icon-->
                                                                        </span>
                                                                    </button>
                                                                    <button type="button" class="btn btn-primary font-weight-bolder font-size-h6 pl-8 pr-4 py-4 my-3" data-wizard-type="action-next">
                                                                        Next
                                                                        <span class="svg-icon svg-icon-md ml-2"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Right-2.svg-->
                                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                                    <polygon points="0 0 24 0 24 24 0 24"/>
                                                                                    <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000) " x="7.5" y="7.5" width="2" height="9" rx="1"/>
                                                                                    <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997) "/>
                                                                                </g>
                                                                            </svg><!--end::Svg Icon-->
                                                                        </span>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        <!--end: Wizard Actions-->
                                                    </form>
                                                    <!--end::Form-->
                                                </div>
                                            <!--end::Signin-->
                                        </div>
                                    <!--end::Container-->
                                </div>
                            <!--begin::Content-->

                            <!--begin::Aside-->
                                <div class="login-aside order-1 order-lg-2 bgi-no-repeat bgi-position-x-right">
                                    <div class="login-conteiner bgi-no-repeat bgi-position-x-right bgi-position-y-bottom" style="background-image: url({{ asset('media/svg/illustrations/login-visual-4.svg') }});">
                                        <!--begin::Aside title-->
                                        <h3 class="pt-lg-40 pl-lg-20 pb-lg-0 pl-10 py-20 m-0 d-flex justify-content-lg-start font-weight-boldest display5 display1-lg text-white">
                                            TIME<br/>
                                            to change<br/>
                                            LIVES
                                        </h3>
                                        <!--end::Aside title-->
                                    </div>
                                </div>
                            <!--end::Aside-->
                        </div>
                    <!--end::Login-->
                </div>
            <!--end::Main-->


            <script>var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";</script>
            <!--begin::Global Config(global config for global JS scripts)-->
                <script>
                    var KTAppSettings = {
                        "breakpoints": {
                            "sm": 576,
                            "md": 768,
                            "lg": 992,
                            "xl": 1200,
                            "xxl": 1200
                        },
                        "colors": {
                            "theme": {
                                "base": {
                                    "white": "#ffffff",
                                    "primary": "#6993FF",
                                    "secondary": "#E5EAEE",
                                    "success": "#1BC5BD",
                                    "info": "#8950FC",
                                    "warning": "#FFA800",
                                    "danger": "#F64E60",
                                    "light": "#F3F6F9",
                                    "dark": "#212121"
                                },
                                "light": {
                                    "white": "#ffffff",
                                    "primary": "#E1E9FF",
                                    "secondary": "#ECF0F3",
                                    "success": "#C9F7F5",
                                    "info": "#EEE5FF",
                                    "warning": "#FFF4DE",
                                    "danger": "#FFE2E5",
                                    "light": "#F3F6F9",
                                    "dark": "#D6D6E0"
                                },
                                "inverse": {
                                    "white": "#ffffff",
                                    "primary": "#ffffff",
                                    "secondary": "#212121",
                                    "success": "#ffffff",
                                    "info": "#ffffff",
                                    "warning": "#ffffff",
                                    "danger": "#ffffff",
                                    "light": "#464E5F",
                                    "dark": "#ffffff"
                                }
                            },
                            "gray": {
                                "gray-100": "#F3F6F9",
                                "gray-200": "#ECF0F3",
                                "gray-300": "#E5EAEE",
                                "gray-400": "#D6D6E0",
                                "gray-500": "#B5B5C3",
                                "gray-600": "#80808F",
                                "gray-700": "#464E5F",
                                "gray-800": "#1B283F",
                                "gray-900": "#212121"
                            }
                        },
                        "font-family": "Poppins"
                    };
                </script>
            <!--end::Global Config-->

    	    <!--begin::Global Theme Bundle(used by all pages)-->
                <script src="{{ asset('plugins/global/plugins.bundle.js') }}"></script>
                <script src="{{ asset('plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
                <script src="{{ asset('js/scripts.bundle.js') }}"></script>
            <!--end::Global Theme Bundle-->
            <!--begin::Page Scripts(used by this page)-->
                <script src="{{ asset('js/pages/custom/login/login-4.js') }}"></script>
            <!--end::Page Scripts-->

            {{-- BOF :: Page Level Scripts --}}
                <script>
                    $( document ).ready(function() {
                        $(".registerbutton").click(function(e){
                            e.preventDefault();

                            // console.log('submit fortm');
                            $(".register_form").submit();
                        });
                    });
                </script>
            {{-- EOF :: Page Level Scripts --}}
        </body>
    <!--end::Body-->
</html>