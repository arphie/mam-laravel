@extends('layouts.admin')

@section('content')
    <?php
        $reg_userid = Session::get('user_id');
    ?>
    <?php if(!empty($reg_userid)): ?>
        <!--begin::Callout-->
            <div class="container">
                <div class="card card-custom mb-2 bg-diagonal bg-diagonal-light-success">
                    <div class="card-body">
                        <div class="d-flex align-items-center justify-content-between p-4 flex-lg-wrap flex-xl-nowrap">
                            <div class="d-flex flex-column mr-5">
                                <a href="#" class="h4 text-dark text-hover-primary mb-5">
                                    Successfully added a Teacher
                                </a>
                                <p class="text-dark-50">
                                    A notification will be sent to her email
                                </p>
                            </div>
                            <div class="ml-6 ml-lg-0 ml-xxl-6 flex-shrink-0">
                                <a href="/" class="btn font-weight-bolder text-uppercase btn-success py-4 px-6">Go back to dashboard</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!--end::Callout-->
    <?php else: ?>
    <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-4  subheader-transparent " id="kt_subheader">
            <div class=" container  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Details-->
                    <div class="d-flex align-items-center flex-wrap mr-2">

                        <!--begin::Title-->
                            <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">New Teacher</h5>
                        <!--end::Title-->

                        <!--begin::Separator-->
                            <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-5 bg-gray-200"></div>
                        <!--end::Separator-->

                        <!--begin::Search Form-->
                            <div class="d-flex align-items-center" id="kt_subheader_search">
                                <span class="text-dark-50 font-weight-bold" id="kt_subheader_total">Enter user details and submit</span>
                            </div>
                        <!--end::Search Form-->

                    </div>
                <!--end::Details-->
            </div>
        </div>
    <!--end::Subheader-->

    <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
                <div class=" container ">
                    <!--begin::Card-->
                        <div class="card card-custom card-transparent">
                            <div class="card-body p-0">
                                <!--begin::Wizard-->
                                    <div class="wizard wizard-4" id="kt_wizard" data-wizard-state="step-first" data-wizard-clickable="true">
                                        <!--begin::Wizard Nav-->
                                            <div class="wizard-nav">
                                                <div class="wizard-steps">
                                                    <div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
                                                        <div class="wizard-wrapper">
                                                            <div class="wizard-number">1</div>
                                                            <div class="wizard-label">
                                                                <div class="wizard-title">Teachers Profile</div>
                                                                <div class="wizard-desc">User's Personal Information</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="wizard-step" data-wizard-type="step">
                                                        <div class="wizard-wrapper">
                                                            <div class="wizard-number">2</div>
                                                            <div class="wizard-label">
                                                                <div class="wizard-title">Affiliation</div>
                                                                <div class="wizard-desc">User's Account & Settings</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wizard-step" data-wizard-type="step">
                                                        <div class="wizard-wrapper">
                                                            <div class="wizard-number">4</div>
                                                            <div class="wizard-label">
                                                                <div class="wizard-title">Submission</div>
                                                                <div class="wizard-desc">Review and Submit</div>
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                </div>
                                            </div>
                                        <!--end::Wizard Nav-->

                                        <!--begin::Card-->
                                            <div class="card card-custom card-shadowless rounded-top-0">
                                                <!--begin::Body-->
                                                    <div class="card-body p-0">
                                                        <div class="row justify-content-center py-8 px-8 py-lg-15 px-lg-10">
                                                            <div class="col-xl-12 col-xxl-10">
                                                                <!--begin::Wizard Form-->
                                                                    <form  action="/teacher/add" method="post"  class="form addteacherform" id="kt_form">
                                                                        {{ csrf_field() }}
                                                                        <div class="row justify-content-center">
                                                                            <div class="col-xl-9">
                                                                                <!--begin::Wizard Step 1-->
                                                                                    <div class="my-5 step" data-wizard-type="step-content" data-wizard-state="current">
                                                                                        <h5 class="text-dark font-weight-bold mb-10">Teacher's Details:</h5>
                                                                                        <!--begin::Group-->
                                                                                            <!-- <div class="form-group row">
                                                                                                <label class="col-xl-3 col-lg-3 col-form-label text-left">Avatar</label>
                                                                                                <div class="col-lg-9 col-xl-9">
                                                                                                    <div class="image-input image-input-outline" id="kt_user_add_avatar">
                                                                                                        <div class="image-input-wrapper" style="background-image: url(assets/media/users/100_6.jpg)"></div>

                                                                                                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                                                                                            <i class="fa fa-pen icon-sm text-muted"></i>
                                                                                                            <input type="file" name="profile_avatar" accept=".png, .jpg, .jpeg"/>
                                                                                                            <input type="hidden" name="profile_avatar_remove"/>
                                                                                                        </label>

                                                                                                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                                                                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                                                                                        </span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div> -->
                                                                                        <!--end::Group-->
                                                                                        @isset($error)
                                                                                            <div class="alert alert-primary" role="alert">{{$error}}</div>
                                                                                        @endisset
                                                                                        <!--begin::Group-->
                                                                                            <div class="form-group row">
                                                                                                <label class="col-xl-3 col-lg-3 col-form-label">First Name</label>
                                                                                                <div class="col-lg-9 col-xl-9">
                                                                                                    <input class="form-control form-control-solid form-control-lg add_first_name" name="firstname" type="text" value=""/>
                                                                                                    <div class="d-add-teacher-error d-first-name-error">First name is required</div>
                                                                                                </div>
                                                                                                
                                                                                            </div>
                                                                                        <!--end::Group-->
                                                                                        <!--begin::Group-->
                                                                                            <div class="form-group row">
                                                                                                <label class="col-xl-3 col-lg-3 col-form-label">Last Name</label>
                                                                                                <div class="col-lg-9 col-xl-9">
                                                                                                    <input class="form-control form-control-solid form-control-lg add_last_name" name="lastname" type="text" value=""/>
                                                                                                    <div class="d-add-teacher-error d-last-name-error">Last name is required</div>
                                                                                                </div>
                                                                                            </div>
                                                                                        <!--end::Group-->
                                                                                        <!--begin::Group-->
                                                                                            <div class="form-group row">
                                                                                                <label class="col-xl-3 col-lg-3 col-form-label">Middle Name</label>
                                                                                                <div class="col-lg-9 col-xl-9">
                                                                                                    <input class="form-control form-control-solid form-control-lg add_middle_name" name="middlename" type="text" value=""/>
                                                                                                    <div class="d-add-teacher-error d-middle-name-error">Middle name is required</div>
                                                                                                </div>
                                                                                            </div>
                                                                                        <!--end::Group-->
                                                                                        <!--begin::Group-->
                                                                                            <div class="form-group row">
                                                                                                <label class="col-xl-3 col-lg-3 col-form-label">Contact Number</label>
                                                                                                <div class="col-lg-9 col-xl-9">
                                                                                                    <div class="input-group input-group-solid input-group-lg">
                                                                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                                                                                        <input type="text" class="form-control form-control-solid form-control-lg add_contact_number" name="teacher_number" value="" placeholder="Phone" />
                                                                                                        
                                                                                                    </div>
                                                                                                    <span class="form-text text-muted">Enter valid US phone number(e.g: 5678967456).</span>
                                                                                                    <div class="d-add-teacher-error  d-contact-number-error">Contact number is required</div>
                                                                                                </div>
                                                                                            </div>
                                                                                        <!--end::Group-->
                                                                                        <!--begin::Group-->
                                                                                            <div class="form-group row">
                                                                                                <label class="col-xl-3 col-lg-3 col-form-label">Email Address</label>
                                                                                                <div class="col-lg-9 col-xl-9">
                                                                                                    <div class="input-group input-group-solid input-group-lg">
                                                                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-at"></i></span></div>
                                                                                                        <input type="text" class="form-control form-control-solid form-control-lg add_email_address" name="email" value=""/>
                                                                                                    </div>
                                                                                                    <div class="d-add-teacher-error  d-email-address-error">Email address is required</div>
                                                                                                </div>
                                                                                            </div>
                                                                                        <!--end::Group-->
                                                                                    </div>
                                                                                <!--end::Wizard Step 1-->

                                                                                <!--begin::Wizard Actions-->
                                                                                    <div class="d-flex justify-content-between border-top pt-10 mt-15">
                                                                                        <div class="mr-2">
                                                                                            <!-- <button type="button" id="prev-step" class="btn btn-light-primary font-weight-bolder px-9 py-4" data-wizard-type="action-prev">Previous</button> -->
                                                                                        </div>
                                                                                        <div>
                                                                                            <a href="/" class="btn btn-danger font-weight-bolder px-9 py-4">Cancel</a>
                                                                                            <button type="button" class="btn btn-success font-weight-bolder px-9 py-4 subsregform" data-wizard-type="action-submit">Submit</button>
                                                                                            <!-- <button type="button" id="next-step" class="btn btn-primary font-weight-bolder px-9 py-4" data-wizard-type="action-next">Next</button> -->
                                                                                        </div>
                                                                                    </div>
                                                                                <!--end::Wizard Actions-->
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                <!--end::Wizard Form-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                <!--end::Body-->
                                            </div>
                                        <!--end::Card-->
                                    </div>
                                <!--end::Wizard-->
                            </div>
                        </div>
                    <!--end::Card-->
                </div>
            <!--end::Container-->
        </div>
    <!--end::Entry-->
    <?php endif; ?>
@endsection

@section('scripts')
    <script>
        $( document ).ready(function() {

            // data population


            // submittion
            $(".subsregform").click(function(e){
                e.preventDefault();

                let fname = $(".add_first_name").val();
                let lname = $(".add_last_name").val();
                let mname = $(".add_middle_name").val();
                let contact_number = $(".add_contact_number").val();
                let demail = $(".add_email_address").val();

                $(".d-first-name-error").hide();
                $(".d-last-name-error").hide();
                $(".d-middle-name-error").hide();
                $(".d-contact-number-error").hide();
                $(".d-email-address-error").hide();

                let is_error = false;

                console.log('first name -> ', fname);
                if(fname == ''){
                    $(".d-first-name-error").show();
                    is_error = true;
                }

                if(lname == ''){
                    $(".d-last-name-error").show();
                    is_error = true;
                }

                if(mname == ''){
                    $(".d-middle-name-error").show();
                    is_error = true;
                }

                if(contact_number == ''){
                    $(".d-contact-number-error").show();
                    is_error = true;
                }

                if(demail == ''){
                    $(".d-email-address-error").show();
                    is_error = true;
                }

                if(is_error){
                    return;
                }

                // console.log('submit now');
                $(".addteacherform").submit();
            });
        });
    </script>
@endsection