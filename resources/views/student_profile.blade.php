@extends('layouts.admin')

@section('content')
    <!--begin::Subheader-->
        <div class="subheader py-3 py-lg-8  subheader-transparent " id="kt_subheader">
            <div class=" container  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                    <div class="d-flex align-items-center flex-wrap mr-1">
                        <!--begin::Mobile Toggle-->
                            <button class="burger-icon burger-icon-left mr-4 d-inline-block d-lg-none" id="kt_subheader_mobile_toggle">
                                <span></span>
                            </button>
                        <!--end::Mobile Toggle-->

                        <!--begin::Page Heading-->
                            <div class="d-flex align-items-baseline flex-wrap mr-5">
                                <!--begin::Page Title-->
                                <h2 class="subheader-title text-dark font-weight-bold my-1 mr-3">{{$student_info['first_name']}}'s Profile</h2>
                                <!--end::Page Title-->

                                <!--begin::Breadcrumb-->
                                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                                        <li class="breadcrumb-item">
                                            <a href="/teacher/profile/{{$teacher_info['id']}}?tab=overview" class="text-muted">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <a href="" class="text-muted">Students</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <a href="#" class="text-muted">Personal Information</a>
                                        </li>
                                    </ul>
                                <!--end::Breadcrumb-->
                            </div>
                        <!--end::Page Heading-->
                    </div>
                <!--end::Info-->
            </div>
        </div>
    <!--end::Subheader-->

    <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class=" container ">
                <!--begin::Profile Personal Information-->
                    <div class="d-flex flex-row">
                        {{-- BOF // sidebar --}}
                        @include('layouts.sections.segments.profile.student_sidebar')
                        {{-- EOF // sidebar --}}
                        <!--begin::Content-->
                            <div class="flex-row-fluid ml-lg-8">
                                @if(app('request')->input('tab') == 'personal')
                                    <!--begin::Card-->
                                        <div class="card card-custom card-stretch">
                                            <!--begin::Header-->
                                                <div class="card-header py-3">
                                                    <div class="card-title align-items-start flex-column">
                                                        <h3 class="card-label font-weight-bolder text-dark">Personal Information</h3>
                                                        <span class="text-muted font-weight-bold font-size-sm mt-1">{{$student_info['first_name']}}'s personal informaiton</span>
                                                    </div>
                                                    {{-- <div class="card-toolbar">
                                                        <button type="reset" class="btn btn-success mr-2">Save Changes</button>
                                                        <button type="reset" class="btn btn-secondary">Cancel</button>
                                                    </div> --}}
                                                </div>
                                            <!--end::Header-->

                                            <!--begin::Form-->
                                                <form class="form">
                                                    <!--begin::Body-->
                                                    <div class="card-body">
                                                        <div class="form-group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">First Name</label>
                                                            <div class="col-lg-9 col-xl-6">
                                                                <input class="form-control form-control-lg form-control-solid" type="text" value="{{$student_info['first_name']}}" readonly/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">Address</label>
                                                            <div class="col-lg-9 col-xl-6">
                                                                <textarea class="form-control form-control-lg form-control-solid" readonly>{{$student_info['address']}}</textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">Birthdate</label>
                                                            <div class="col-lg-9 col-xl-6">
                                                                <input class="form-control form-control-lg form-control-solid" type="text" value="{{$student_info['birth_date']}}" readonly/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">Email Address</label>
                                                            <div class="col-lg-9 col-xl-6">
                                                                <div class="input-group input-group-lg input-group-solid">
                                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-at"></i></span></div>
                                                                    <input type="text" class="form-control form-control-lg form-control-solid" value="{{$student_info['email']}}" placeholder="{{$student_info['email']}}" readonly/>
                                                                </div>
                                                                <span class="form-text text-muted">We'll never share your email with anyone else.</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">Contact Phone</label>
                                                            <div class="col-lg-9 col-xl-6">
                                                                <div class="input-group input-group-lg input-group-solid">
                                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                                                    <input type="text" class="form-control form-control-lg form-control-solid" value="{{$student_info['contact_number']}}" readonly/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {{-- <div class="row">
                                                            <label class="col-xl-3"></label>
                                                            <div class="col-lg-9 col-xl-6">
                                                                <h5 class="font-weight-bold mt-10 mb-6">Incase of Emergency</h5>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">First Name</label>
                                                            <div class="col-lg-9 col-xl-6">
                                                                <input class="form-control form-control-lg form-control-solid" type="text" value="" readonly/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">Contact Phone</label>
                                                            <div class="col-lg-9 col-xl-6">
                                                                <div class="input-group input-group-lg input-group-solid">
                                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                                                    <input type="text" class="form-control form-control-lg form-control-solid" value="" placeholder="Phone" readonly />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-xl-3 col-lg-3 col-form-label">Relationship</label>
                                                            <div class="col-lg-9 col-xl-6">
                                                                <input class="form-control form-control-lg form-control-solid" type="text" value="" readonly/> 
                                                            </div>
                                                        </div> --}}
                                                    </div>
                                                    <!--end::Body-->
                                                </form>
                                            <!--end::Form-->
                                        </div>
                                    <!--end::Card-->
                                @elseif(app('request')->input('tab') == 'sentenses')
                                <div class="card card-custom card-stretch">
                                        <!--begin::Header-->
                                            <div class="card-header py-3">
                                                <div class="card-title align-items-start flex-column">
                                                    <h3 class="card-label font-weight-bolder text-dark">PECS Sentenses</h3>
                                                    <span class="text-muted font-weight-bold font-size-sm mt-1">{{$student_info['first_name']}}'s personal PECS Board Sentenses</span>
                                                    <span class="text-muted font-weight-bold font-size-sm mt-1"><a href="/export/{{$student_info['id']}}/sentenses">Export Values</a></span>
                                                </div>
                                            </div>
                                        <!--end::Header-->

                                        <!--begin::Form-->
                                            <div class="card-body">
                                                <div class="accordion list-of-correct-sentenses accordion-solid accordion-toggle-plus" id="accordionExample3">
                                                    @foreach($sentenses as $sentense_key => $svalue)
                                                        <div class="d-sentense-item">
                                                            <div class="d-sentense-left">
                                                                <div class="ddate">
                                                                    @if($svalue['iscorrect'] == 'correct')
                                                                        <span class="d-correct-badge">&#10004;</span>
                                                                    @endif
                                                                    {{$svalue['name']}}
                                                                </div>
                                                                <div class="dtext">{{$svalue['created']}}</div>
                                                                
                                                            </div>
                                                            <div class="d-sentense-right">
                                                                @if($active_user['access_level'] != 1)
                                                                    @if($svalue['iscorrect'] != 'correct')
                                                                        <a href="/students/profile/{{$student_info['id']}}/cs/{{$svalue['name']}}" class="btn mark-correct">mark as correct</a>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        <!--end::Form-->
                                    </div>
                                @elseif(app('request')->input('tab') == 'board')
                                    <!--begin::Card-->
                                    <div class="card card-custom card-stretch">
                                        <!--begin::Header-->
                                            <div class="card-header py-3">
                                                <div class="card-title align-items-start flex-column">
                                                    <h3 class="card-label font-weight-bolder text-dark">PECS Board</h3>
                                                    <span class="text-muted font-weight-bold font-size-sm mt-1">{{$student_info['first_name']}}'s personal PECS Board</span>
                                                </div>
                                                <div class="card-toolbar">
                                                    @if($active_user['access_level'] != 1)
                                                        <a href="/students/profile/tiles/{{$student_info['id']}}/add" class="btn btn-success mr-2"><i class="fas fa-plus-circle"></i> Intoduce Tile</a>
                                                        {{-- <button type="reset" class="btn btn-success"><i class="fas fa-plus-circle"></i> Category</button> --}}
                                                    @endif
                                                    

                                                    <!-- Modal-->
                                                </div>
                                            </div>
                                        <!--end::Header-->

                                        <!--begin::Form-->
                                            <div class="card-body">
                                                <div class="accordion accordion-solid accordion-toggle-plus" id="accordionExample3">
                                                    @foreach($board_info as $board_key => $board)
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseboard{{$board_key}}">
                                                                    {{$board['name']}}
                                                                </div>
                                                            </div>
                                                            <div id="collapseboard{{$board_key}}" class="collapse" data-parent="#accordionExample3">
                                                                <div class="card-body">
                                                                    <div class="d-board-tiles">
                                                                        @foreach($board['items'] as $tile_key => $tile)
                                                                            <!--begin::Item-->
                                                                                <div class="d-flex flex-wrap align-items-center mb-10">
                                                                                    <!--begin::Symbol-->
                                                                                    <div class="symbol symbol-60 symbol-2by3 flex-shrink-0 mr-4">
                                                                                        <div class="symbol-label" style="background-image: url('{{ asset($tile['tile']) }}')"></div>
                                                                                    </div>
                                                                                    <!--end::Symbol-->
                                                                                    <!--begin::Title-->
                                                                                    <div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
                                                                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">{{$tile['name']}}</a>
                                                                                        {{-- <span class="text-muted font-weight-bold font-size-sm my-1">{{$tile['item_text']}}</span> --}}
                                                                                        {{-- <span class="text-muted font-weight-bold font-size-sm">Added last <span class="text-primary font-weight-bold">January 01, 2021</span></span> --}}
                                                                                    </div>
                                                                                    <!--end::Title-->
                                                                                    <!--begin::Info-->
                                                                                    <div class="d-flex align-items-center py-lg-0 py-2">
                                                                                        <div class="d-flex flex-column text-right">
                                                                                            {{-- <span class="text-dark-75 font-weight-bolder font-size-h4"></span> --}}
                                                                                            @if($active_user['access_level'] != 1)
                                                                                            <a href="/students/profile/1/delete/{{$tile['id']}}">Delete</a>
                                                                                            @endif
                                                                                            {{-- <span class="text-muted font-size-sm font-weight-bolder">100 correct of 200 attempts</span> --}}
                                                                                        </div>
                                                                                    </div>
                                                                                    <!--end::Info-->
                                                                                </div>
                                                                            <!--end::Item-->
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        <!--end::Form-->
                                    </div>
                                <!--end::Card-->
                                @else
                                    <!--begin::Card-->
                                    <div class="card card-custom gutter-b">
                                        <!--begin::Header-->
                                            <div class="card-header py-3">
                                                <div class="align-items-start flex-column">
                                                    <h3 class="card-label font-weight-bolder text-dark">Dashboard</h3>
                                                    <span class="text-muted font-weight-bold font-size-sm mt-1">{{$student_info['first_name']}}'s analytical informaiton</span>
                                                </div>
                                                {{-- <div class="card-toolbar">
                                                    <button type="reset" class="btn btn-success mr-2">Save Changes</button>
                                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                                </div> --}}
                                            </div>
                                        <!--end::Header-->
                                            <!--begin::Body-->
                                            <div class="card-body">
                                                <!--begin: Items-->
                                                <div class="d-flex align-items-center flex-wrap">
                                                    <!--begin: Item-->
                                                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                        <span class="mr-4">
                                                            <i class="flaticon-network icon-2x text-muted font-weight-bold"></i>
                                                        </span>
                                                        <div class="d-flex flex-column text-dark-75">
                                                            <span class="font-weight-bolder font-size-sm">Total Attempts</span>
                                                            <span class="font-weight-bolder font-size-h5">
                                                            <span class="text-dark-50 font-weight-bold"></span>{{$tile_stats['attempts']}}</span>
                                                        </div>
                                                    </div>
                                                    <!--end: Item-->
                                                    <!--begin: Item-->
                                                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                        <span class="mr-4">
                                                            <i class="flaticon-medal icon-2x text-muted font-weight-bold"></i>
                                                        </span>
                                                        <div class="d-flex flex-column text-dark-75">
                                                            <span class="font-weight-bolder font-size-sm">Correct Sentense</span>
                                                            <span class="font-weight-bolder font-size-h5">
                                                            <span class="text-dark-50 font-weight-bold"></span>{{$tile_stats['correct']}}</span>
                                                        </div>
                                                    </div>
                                                    <!--end: Item-->
                                                    <!--begin: Item-->
                                                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                        <span class="mr-4">
                                                            <i class="flaticon-diagram icon-2x text-muted font-weight-bold"></i>
                                                        </span>
                                                        <div class="d-flex flex-column text-dark-75">
                                                            <span class="font-weight-bolder font-size-sm">Success Rate</span>
                                                            <span class="font-weight-bolder font-size-h5">
                                                            <span class="text-dark-50 font-weight-bold"></span>{{$tile_stats['success']}}%</span>
                                                        </div>
                                                    </div>
                                                    <!--end: Item-->
                                                    <!--begin: Item-->
                                                    <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
                                                        <span class="mr-4">
                                                            <i class="flaticon-squares-1 icon-2x text-muted font-weight-bold"></i>
                                                        </span>
                                                        <div class="d-flex flex-column flex-lg-fill">
                                                            <span class="text-dark-75 font-weight-bolder font-size-sm">{{$tile_stats['tiles_count']}} Active Tiles</span>
                                                            <a href="/students/profile/{{$student_info['id']}}?tab=board" class="text-primary font-weight-bolder">View</a>
                                                        </div>
                                                    </div>
                                                    <!--end: Item-->
                                                </div>
                                                <!--begin: Items-->
                                            </div>
                                            <!--end::Body-->
                                    </div>
                                    
                                    <div class="row">
                                    <div class="col-xl-6">
                                        <!--begin::List Widget 7-->
                                            <div class="card card-custom card-stretch gutter-b card-shadowless">
                                                <!--begin::Header-->
                                                <div class="card-header border-0">
                                                    <h3 class="card-title font-weight-bolder text-dark">Notes</h3>
                                                </div>
                                                <!--end::Header-->
                                                <!--begin::Body-->
                                                <div class="card-body d-per-day-report pt-0">
                                                    @if($active_user['access_level'] != 1)
                                                        <div class="d-notes-list">
                                                            <div class="dtextarea">
                                                                <form  action="/students/profile/add/notes" method="post">
                                                                    {{ csrf_field() }}
                                                                    <div class="d-note-input-field">
                                                                        <textarea name="d-student-note" id="" cols="30" rows="10" placeholder="Enter remarks.."></textarea>
                                                                    </div>
                                                                    <div class="d-notes-input-button">
                                                                        <input type="hidden" name="student_id" value="{{$student_info['id']}}">
                                                                        <input type="submit" value="save">
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    <div class="d-notes-list">
                                                        <div class="d-notes-list-inner">
                                                        @foreach($notes as $nkey => $nvalue)
                                                            <div class="d-notes-list-item">
                                                                <div class="d-note-date"><span>{{$nvalue['created_at']}}</span></div>
                                                                <div class="d-note-text">{{$nvalue['snotes']}}</div>
                                                            </div>
                                                        @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end::Body-->
                                            </div>
                                            <!--end::List Widget 7-->
                                    </div>
                                    <div class="col-xl-6">
                                            <!--begin::List Widget 7-->
                                                <div class="card card-custom card-stretch gutter-b card-shadowless">
                                                    <!--begin::Header-->
                                                    <div class="card-header border-0">
                                                        <h3 class="card-title font-weight-bolder text-dark">Daily Count</h3>
                                                    </div>
                                                    <!--end::Header-->
                                                    <!--begin::Body-->
                                                    <div class="card-body d-per-day-report pt-0">
                                                        <div class="d-per-day-notes">Daily usage of app</div>
                                                        @foreach($tile_stats['per_day'] as $key => $value)
                                                            <!--begin::Item-->
                                                                <div class="d-vertical-cols-sentense">
                                                                    <!--begin::Text-->
                                                                    <div class="d-per-day-label">
                                                                        <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">{{$key}}</a>
                                                                    </div>
                                                                    <div class="d-per-day">
                                                                        <div class="d-per-day-placeholder">
                                                                            <div class="dcolsvertical" style="width: {{ $value['svals'] }}%">&nbsp;</div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="d-per-day-number">
                                                                        <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">{{$value['dcount']}}</a>
                                                                    </div>
                                                                    <!--end::Text-->
                                                                </div>
                                                            <!--end::Item-->
                                                        @endforeach
                                                    </div>
                                                    <!--end::Body-->

                                                    <!--begin::Header-->
                                                    <div class="card-header border-0">
                                                        <h3 class="card-title font-weight-bolder text-dark">Daily Correct Count</h3>
                                                    </div>
                                                    <!--end::Header-->
                                                    <!--begin::Body-->
                                                    <div class="card-body d-per-day-report pt-0">
                                                        <div class="d-per-day-notes">Correct sentense build per day of the week</div>
                                                        @foreach($tile_stats['correct_day'] as $key => $value)
                                                            <!--begin::Item-->
                                                                <div class="d-vertical-cols-sentense">
                                                                    <!--begin::Text-->
                                                                    <div class="d-per-day-label">
                                                                        <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">{{$key}}</a>
                                                                    </div>
                                                                    <div class="d-per-day">
                                                                        <div class="d-per-day-placeholder">
                                                                            <div class="dcolsvertical" style="width: {{ $value['svals'] }}%">&nbsp;</div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="d-per-day-number">
                                                                        <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">{{$value['dcount']}}</a>
                                                                    </div>
                                                                    <!--end::Text-->
                                                                </div>
                                                            <!--end::Item-->
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <!--end::List Widget 7-->
                                        </div>
                                        <div class="col-xl-6">
                                            <!--begin::List Widget 7-->
                                                <div class="card card-custom card-stretch gutter-b card-shadowless">
                                                    <!--begin::Header-->
                                                    <div class="card-header border-0">
                                                        <h3 class="card-title font-weight-bolder text-dark">Top Tiles</h3>
                                                    </div>
                                                    <!--end::Header-->
                                                    <!--begin::Body-->
                                                    <div class="card-body pt-0">
                                                        @foreach($tile_stats['top_tiles'] as $key => $value)
                                                            <!--begin::Item-->
                                                                <div class="d-flex align-items-center flex-wrap mb-10">
                                                                    <!--begin::Symbol-->
                                                                    <div class="symbol symbol-50 symbol-light mr-5">
                                                                        <span class="symbol-label">
                                                                            <img src="{{ asset($value['tile']) }}" class="h-50 align-self-center" alt="" />
                                                                        </span>
                                                                    </div>
                                                                    <!--end::Symbol-->
                                                                    <!--begin::Text-->
                                                                    <div class="d-flex flex-column flex-grow-1 mr-2">
                                                                        <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">{{$value['name']}}</a>
                                                                        {{-- <span class="text-muted font-weight-bold">Categoty 1</span> --}}
                                                                    </div>
                                                                    <!--end::Text-->
                                                                    <span class="label label-xl label-light label-inline my-lg-0 my-2 text-dark-50 font-weight-bolder">{{$value['count']}} times</span>
                                                                </div>
                                                            <!--end::Item-->
                                                        @endforeach
                                                    </div>
                                                    <!--end::Body-->
                                                </div>
                                                <!--end::List Widget 7-->
                                        </div>

                                        <div class="col-xl-6">
                                            <!--begin::List Widget 7-->
                                                <div class="card card-custom card-stretch gutter-b card-shadowless">
                                                    <!--begin::Header-->
                                                    <div class="card-header border-0">
                                                        <h3 class="card-title font-weight-bolder text-dark">Recent Sentenses</h3>
                                                    </div>
                                                    <!--end::Header-->
                                                    <!--begin::Body-->
                                                    <div class="card-body pt-0">
                                                        @foreach($tile_stats['sentenses'] as $key => $value)
                                                            <!--begin::Item-->
                                                                <div class="d-flex align-items-center flex-wrap mb-10">
                                                                    <!--begin::Symbol-->
                                                                    {{-- <div class="symbol symbol-50 symbol-light mr-5">
                                                                        <span class="symbol-label">
                                                                            <img src="{{ asset('/media/logos/logo-letter-1.png') }}" class="h-50 align-self-center" alt="" />
                                                                        </span>
                                                                    </div> --}}
                                                                    <!--end::Symbol-->
                                                                    <!--begin::Text-->
                                                                    <div class="d-flex flex-column flex-grow-1 mr-2">
                                                                        <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1">{{$value['sentense']}}</a>
                                                                        {{-- <span class="text-muted font-weight-bold">Categoty 1</span> --}}
                                                                    </div>
                                                                    <!--end::Text-->
                                                                    <span class="label label-xl label-light label-inline my-lg-0 my-2 text-dark-50 font-weight-bolder">{{$value['created_at']}}</span>
                                                                </div>
                                                            <!--end::Item-->
                                                        @endforeach
                                                    </div>
                                                    <!--end::Body-->
                                                </div>
                                                <!--end::List Widget 7-->
                                        </div>
                                        
                                        {{-- <div class="col-xl-6">
                                            <div class="card card-custom card-stretch gutter-b">
                                                <div class="card-header h-auto border-0">
                                                    <div class="card-title py-5">
                                                        <h3 class="card-label">
                                                            <span class="d-block text-dark font-weight-bolder">Monthly Activities</span>
                                                            <span class="d-block text-muted mt-2 font-size-sm">Board is used more than 500+ times</span>
                                                        </h3>
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <div id="kt_charts_widget_3_chart"></div>
                                                </div>
                                            </div>
                                        </div> --}}
                                        
                                    </div>
                                <!--end::Card-->
                                @endif
                                


                            </div>
                        <!--end::Content-->
                    </div>
                <!--end::Profile Personal Information-->
            </div>
            <!--end::Container-->
        </div>
    <!--end::Entry-->
@endsection
