@extends('layouts.admin')

@section('content')
    <!--begin::Subheader-->
        <div class="subheader py-3 py-lg-8  subheader-transparent " id="kt_subheader">
            <div class=" container  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                    <div class="d-flex align-items-center flex-wrap mr-1">
                        <!--begin::Mobile Toggle-->
                            <button class="burger-icon burger-icon-left mr-4 d-inline-block d-lg-none" id="kt_subheader_mobile_toggle">
                                <span></span>
                            </button>
                        <!--end::Mobile Toggle-->

                        <!--begin::Page Heading-->
                            <div class="d-flex align-items-baseline flex-wrap mr-5">
                                <!--begin::Page Title-->
                                <h2 class="subheader-title text-dark font-weight-bold my-1 mr-3">{{$student_info['first_name']}}'s Profile</h2>
                                <!--end::Page Title-->

                                <!--begin::Breadcrumb-->
                                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                                        <li class="breadcrumb-item">
                                            <a href="/teacher/profile/{{$teacher_info['id']}}?tab=overview" class="text-muted">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <a href="" class="text-muted">Students</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <a href="#" class="text-muted">Personal Information</a>
                                        </li>
                                    </ul>
                                <!--end::Breadcrumb-->
                            </div>
                        <!--end::Page Heading-->
                    </div>
                <!--end::Info-->
            </div>
        </div>
    <!--end::Subheader-->

    <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class=" container ">
                <!--begin::Profile Personal Information-->
                    <div class="d-flex flex-row">
                        {{-- BOF // sidebar --}}
                        @include('layouts.sections.segments.profile.student_sidebar')
                        {{-- EOF // sidebar --}}
                        <!--begin::Content-->
                            <div class="flex-row-fluid ml-lg-8">
                                <!--begin::Card-->
                                <div class="card card-custom card-stretch">
                                    <!--begin::Header-->
                                        <div class="card-header py-3">
                                            <div class="card-title align-items-start flex-column">
                                                <h3 class="card-label font-weight-bolder text-dark">Introduce Tiles</h3>
                                                <span class="text-muted font-weight-bold font-size-sm mt-1">Add Tiles on {{$student_info['first_name']}}'s personal pecs board</span>
                                            </div>
                                            <div class="card-toolbar">
                                                <button type="reset" id="save_tile" class="btn btn-success mr-2">Save Changes</button>
                                                <a href="/students/profile/{{$student_info['id']}}?tab=board" class="btn btn-secondary">Cancel</a>
                                            </div>
                                        </div>
                                    <!--end::Header-->

                                    <!--begin::Form-->
                                        <form class="form" id="update_student_tile" action="/students/profile/tiles/{{$student_info['id']}}/add" method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <!--begin::Body-->
                                            <div class="card-body">
                                                <div class="alert alert-primary show_if_has_error" style="display: none" role="alert">
                                                    All fields are required
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Tile Name</label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <input class="form-control form-control-lg form-control-solid dtilename" type="text" name="name" value=""/>
                                                        <input type="hidden" name="userid" value="{{$student_info['id']}}"/>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Category</label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <input class="form-control form-control-lg form-control-solid dtilecategory" type="text" name="category" value=""/>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Order</label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <input class="form-control form-control-lg form-control-solid dtilecategory" type="number" name="order" value=""/>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Tiles</label>
                                                    <div class="col-lg-9 col-xl-6" data-scroll="true" data-height="500">
                                                        <div class="d-all-tiles">
                                                            <div class="row">
                                                                @foreach ($tiles as $item)
                                                                    <div class="col-lg-3">
                                                                        <div class="symbol symbol-60 symbol-xxl-100 mr-5 align-self-start align-self-xxl-center pecs_tile" data-tile="{{ asset($item) }}" style="cursor: pointer">
                                                                            <div class="symbol-label" style="background-image:url('{{ asset($item) }}')"></div>
                                                                            {{-- <i class="symbol-badge bg-success"></i> --}}
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="tile" id="selected_tile" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">&nbsp;</label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        {{-- <input type="hidden" name="tile" id="selected_tile" value=""> --}}
                                                        <h4>or upload new tile</h4>
                                                        <input type="file" name="newtile" id="" >
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end::Body-->
                                        </form>
                                    <!--end::Form-->
                                </div>
                            <!--end::Card-->
                                


                            </div>
                        <!--end::Content-->
                    </div>
                <!--end::Profile Personal Information-->
            </div>
            <!--end::Container-->
        </div>
    <!--end::Entry-->
@endsection

@section('scripts')
    <script>
		$( document ).ready(function() {
			console.log( "ready!" );

            $("#save_tile").click(function(e){
                e.preventDefault();

                console.log('save title');

                // title
                let tile_name = $(".dtilename").val();
                console.log('title ->', tile_name);

                // category
                let category = $(".dtilecategory").val();
                console.log('category ->', category);

                // tile
                // let selected_tile = $("#selected_tile").val();
                // console.log('selected_tile ->', selected_tile);

                if(tile_name == "" || category == "" || selected_tile == "") {
                    $(".show_if_has_error").show();
                    console.log('all is empty');
                    return;
                }

                console.log('proceed');
                $('#update_student_tile').submit();
            });

            $(".pecs_tile").click(function(e){
                e.preventDefault();
                $(".d-all-tiles .pecs_tile").css("opacity",".2");
                $(".pecs_tile").removeClass('selected-tile');

                let selected_tile = $(this).data("tile");
                $(this).addClass('selected-tile');
                $(this).css("opacity","1");
                $("#selected_tile").val(selected_tile);
            });
		});
	</script>
@endsection
