<!DOCTYPE html>
<html lang="en" >
    <!--begin::Head-->
    <head>
        <base href="">
        <meta charset="utf-8"/>
        <title>Dashboard | MAM Assistive</title>
        <meta name="description" content="Updates and statistics"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

        <!--begin::Fonts-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>        <!--end::Fonts-->

		<!--begin::Page Vendors Styles(used by this page)-->
			<link href="{{ asset('plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css"/>
            <link href="{{ asset('/css/pages/wizard/wizard-4.css') }}" rel="stylesheet" type="text/css"/>
        <!--end::Page Vendors Styles-->


        <!--begin::Global Theme Styles(used by all pages)-->
			<link href="{{ asset('plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css"/>
			<link href="{{ asset('plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css"/>
			<link href="{{ asset('css/style.bundle.css') }}" rel="stylesheet" type="text/css"/>
			<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css"/>
		<!--end::Global Theme Styles-->
		
        <link rel="shortcut icon" href="{{ asset('media/logos/favicon.png') }}"/>

    </head>
    <!--end::Head-->