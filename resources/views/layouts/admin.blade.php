@include('layouts.sections.head')
<!--begin::Body-->
    <body  id="kt_body"  class="quick-panel-right demo-panel-right offcanvas-right header-fixed header-mobile-fixed aside-enabled aside-static page-loading"  >

        @include('layouts.sections.segments.mobile_header')
            
            </div>
            <!--end::Header Mobile-->
                <div class="d-flex flex-column flex-root">
                    <!--begin::Page-->
                    <div class="d-flex flex-row flex-column-fluid page">

                        {{-- Begin: menu --}}
                        @include('layouts.sections.segments.menu')
                        {{-- End: menu --}}
                        
                        <!--begin::Wrapper-->
                        <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
                            
                            {{-- Begin: Page Header --}}
                            @include('layouts.sections.segments.header')
                            {{-- Begin: Page header --}}

                            <!--begin::Content-->
                            <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">

                                @yield('content')

                            </div>
                            <!--end::Content-->

                            {{-- Begin: Page Footer --}}
                            @include('layouts.sections.segments.footer')
                            {{-- Begin: Page Footer --}}

                        </div>
                        <!--end::Wrapper-->
                    </div>
                    <!--end::Page-->
                </div>
            <!--end::Main-->


        {{-- Begin: User info popup --}}
        @include('layouts.sections.components.pop_userinfo')
        {{-- Begin: User info popup --}}

        {{-- Begin: User info popup --}}
        @include('layouts.sections.components.scroll_to_top')
        {{-- Begin: User info popup --}}

        {{-- Begin: Global Footer --}}
        @include('layouts.sections.foot')
        {{-- Begin: Global Footer --}}
    </body>
<!--end::Body-->
</html>