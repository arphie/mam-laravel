<!DOCTYPE html>
<html lang="en" >
    <!--begin::Head-->
        <head>
            <meta charset="utf-8"/>
            <title>Download | MAM Assistive</title>
            <meta name="description" content="Singin page example"/>
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

            <!--begin::Fonts-->
                <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>        <!--end::Fonts-->


            <!--begin::Page Custom Styles(used by this page)-->
                <link href="{{ asset('css/pages/login/login-4.css') }}" rel="stylesheet" type="text/css"/>
            <!--end::Page Custom Styles-->

            <!--begin::Global Theme Styles(used by all pages)-->
                <link href="{{ asset('plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css"/>
                <link href="{{ asset('plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css"/>
                <link href="{{ asset('css/style.bundle.css') }}" rel="stylesheet" type="text/css"/>
            <!--end::Global Theme Styles-->

            <link rel="shortcut icon" href="{{ asset('media/logos/favicon.png') }}"/>
            <style>
                .d-apk-item {
                    margin-bottom: 20px;
                }
            </style>
        </head>
    <!--end::Head-->

    <!--begin::Body-->
        <body  id="kt_body"  class="quick-panel-right demo-panel-right offcanvas-right header-fixed header-mobile-fixed subheader-enabled aside-enabled aside-static page-loading"  >

            <!--begin::Main-->
                <div class="d-flex flex-column flex-root">
                    <!--begin::Login-->
                        <div class="login login-4 wizard d-flex flex-column flex-lg-row flex-column-fluid wizard" id="kt_login">
                            <!--begin::Content-->
                                <div class="login-container d-flex flex-center flex-row flex-row-fluid order-2 order-lg-1 flex-row-fluid bg-white py-lg-0 pb-lg-0 pt-15 pb-12">
                                    <!--begin::Container-->
                                        <div class="login-content login-content-signup d-flex flex-column">
                                            

                                            <!--begin::Signin-->
                                                <div class="login-form">
                                                    <!--begin::Form-->
                                                    <div class="pb-10 pb-lg-12">
                                                        <h3 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Start using MAM</h3>
                                                        <div class="text-muted font-weight-bold font-size-h4">Please select and download an APK</div>
                                                    </div>
                                                    <div class="d-api-list">
                                                        <div class="d-apk-item">
                                                            <div class="d-apk-inner">
                                                                <h4>MAM for Andoid</h4>
                                                                <div class="d-apk-desc">Download and install android installer</div>
                                                                <div class="d-apk-button">
                                                                    <a href="/download/apk">Download Installer</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="d-apk-item">
                                                            <div class="d-apk-inner">
                                                                <h4>MAM for iOS</h4>
                                                                <div class="d-apk-desc">Download and install iOS installer</div>
                                                                <div class="d-apk-button">
                                                                    <a href="/download/ios">Download Installer</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end::Form-->
                                                </div>
                                            <!--end::Signin-->
                                        </div>
                                    <!--end::Container-->
                                </div>
                            <!--begin::Content-->

                            <!--begin::Aside-->
                                <div class="login-aside order-1 order-lg-2 bgi-no-repeat bgi-position-x-right">
                                    <div class="login-conteiner bgi-no-repeat bgi-position-x-right bgi-position-y-bottom" style="background-image: url({{ asset('media/svg/illustrations/login-visual-4.svg') }});">
                                        <!--begin::Aside title-->
                                        <h3 class="pt-lg-40 pl-lg-20 pb-lg-0 pl-10 py-20 m-0 d-flex justify-content-lg-start font-weight-boldest display5 display1-lg text-white">
                                            TIME<br/>
                                            to change<br/>
                                            LIVES
                                        </h3>
                                        <!--end::Aside title-->
                                    </div>
                                </div>
                            <!--end::Aside-->
                        </div>
                    <!--end::Login-->
                </div>
            <!--end::Main-->


            <script>var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";</script>
            <!--begin::Global Config(global config for global JS scripts)-->
                <script>
                    var KTAppSettings = {
                        "breakpoints": {
                            "sm": 576,
                            "md": 768,
                            "lg": 992,
                            "xl": 1200,
                            "xxl": 1200
                        },
                        "colors": {
                            "theme": {
                                "base": {
                                    "white": "#ffffff",
                                    "primary": "#6993FF",
                                    "secondary": "#E5EAEE",
                                    "success": "#1BC5BD",
                                    "info": "#8950FC",
                                    "warning": "#FFA800",
                                    "danger": "#F64E60",
                                    "light": "#F3F6F9",
                                    "dark": "#212121"
                                },
                                "light": {
                                    "white": "#ffffff",
                                    "primary": "#E1E9FF",
                                    "secondary": "#ECF0F3",
                                    "success": "#C9F7F5",
                                    "info": "#EEE5FF",
                                    "warning": "#FFF4DE",
                                    "danger": "#FFE2E5",
                                    "light": "#F3F6F9",
                                    "dark": "#D6D6E0"
                                },
                                "inverse": {
                                    "white": "#ffffff",
                                    "primary": "#ffffff",
                                    "secondary": "#212121",
                                    "success": "#ffffff",
                                    "info": "#ffffff",
                                    "warning": "#ffffff",
                                    "danger": "#ffffff",
                                    "light": "#464E5F",
                                    "dark": "#ffffff"
                                }
                            },
                            "gray": {
                                "gray-100": "#F3F6F9",
                                "gray-200": "#ECF0F3",
                                "gray-300": "#E5EAEE",
                                "gray-400": "#D6D6E0",
                                "gray-500": "#B5B5C3",
                                "gray-600": "#80808F",
                                "gray-700": "#464E5F",
                                "gray-800": "#1B283F",
                                "gray-900": "#212121"
                            }
                        },
                        "font-family": "Poppins"
                    };
                </script>
            <!--end::Global Config-->

    	    <!--begin::Global Theme Bundle(used by all pages)-->
                <script src="{{ asset('plugins/global/plugins.bundle.js') }}"></script>
                <script src="{{ asset('plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
                <script src="{{ asset('js/scripts.bundle.js') }}"></script>
            <!--end::Global Theme Bundle-->
            <!--begin::Page Scripts(used by this page)-->
                <script src="{{ asset('js/pages/custom/login/login-4.js') }}"></script>
            <!--end::Page Scripts-->

            {{-- BOF :: Page Level Scripts --}}
                <script>
                    $( document ).ready(function() {
                        $(".registerbutton").click(function(e){
                            e.preventDefault();

                            // console.log('submit fortm');
                            $(".register_form").submit();
                        });
                    });
                </script>
            {{-- EOF :: Page Level Scripts --}}
        </body>
    <!--end::Body-->
</html>