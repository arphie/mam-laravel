Hello <strong>{{ $name }}</strong>,
<p>This is to inform you that you have been added to the Pecsboard dashboard.</p>
<p>Please follow this account details to login on your dashboard:</p>
<p>email: {{$email}}</p>
<p>password: <strong>{{$pass}}</strong></p>
<p>&nbsp;</p>
<p>Thank you.</p>