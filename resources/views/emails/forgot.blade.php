Hello <strong>{{ $name }}</strong>,
<p>You have requested for a new password in our portal</p>
<p>Please use this new password to access your account:</p>
<p>password: <strong>{{$pass}}</strong></p>
<p>Please make sure to update your password once you successfully logged in</p>
<p>&nbsp;</p>
<p>Thank you.</p>