@extends('layouts.admin')

@section('content')
    <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class=" container ">
                <!--begin::Profile Personal Information-->
                    <div class="d-flex flex-row">
                        {{-- BOF // sidebar --}}
                        @include('layouts.sections.segments.profile.sidebar')
                        {{-- EOF // sidebar --}}
                        <div class="flex-row-fluid ml-lg-8">
                            @if(app('request')->input('tab') == 'personal')
                                <!--begin::Card-->
                                    <div class="card card-custom card-stretch">
                                        <!--begin::Header-->
                                            <div class="card-header py-3">
                                                <div class="card-title align-items-start flex-column">
                                                    <h3 class="card-label font-weight-bolder text-dark">Teachers Personal Information</h3>
                                                    <span class="text-muted font-weight-bold font-size-sm mt-1">Update your personal informaiton</span>
                                                </div>
                                                <div class="card-toolbar">
                                                    <button type="reset" id="save_profile_changes" class="btn btn-success mr-2">Save Changes</button>
                                                    {{-- <button type="reset" class="btn btn-secondary">Cancel</button> --}}
                                                </div>
                                            </div>
                                        <!--end::Header-->

                                        <!--begin::Form-->
                                            <form class="form" id="update_teacher_profile" action="/teacher/profile/update" method="post" enctype="multipart/form-data">
												{{ csrf_field() }}
                                                <!--begin::Body-->
												
                                                <div class="card-body">
													@if(app('request')->input('error') == 'one')
														<div class="alert alert-primary" role="alert" style="margin-bottom: 40px">Something went wrong, please try again.</div>
													@endif
													@if(app('request')->input('success') == 'one')
														<div class="alert alert-success" role="alert" style="margin-bottom: 40px">Successfully saved information</div>
													@endif
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="image-input image-input-outline" id="kt_profile_avatar" style="background-image: url({{ asset('media/users/default.jpg') }})">
                                                                <div class="image-input-wrapper" style="background-image: url({{ asset($profile['profile_avatar']) }})"></div>

                                                                <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                                                    <i class="fa fa-pen icon-sm text-muted"></i>
                                                                    <input type="file" name="profile_avatar" accept=".png, .jpg, .jpeg"/>
                                                                    <input type="hidden" name="profile_avatar"/>
                                                                </label>

                                                                <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                                                    <i class="ki ki-bold-close icon-xs text-muted"></i>
                                                                </span>

                                                                <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="Remove avatar">
                                                                    <i class="ki ki-bold-close icon-xs text-muted"></i>
                                                                </span>
                                                            </div>
                                                            <span class="form-text text-muted">Allowed file types:  png, jpg, jpeg.</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">First Name</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <input class="form-control form-control-lg form-control-solid" name="firstname" type="text" value="{{$profile['first_name']}}"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Last Name</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <input class="form-control form-control-lg form-control-solid" name="lastname" type="text" value="{{$profile['last_name']}}"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Middle Name</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <input class="form-control form-control-lg form-control-solid" name="middlename" type="text" value="{{$profile['middle_name']}}"/>
                                                            {{-- <span class="form-text text-muted">If you want your invoices addressed to a company. Leave blank to use your full name.</span> --}}
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Contact Phone</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="input-group input-group-lg input-group-solid">
                                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                                                <input type="text" class="form-control form-control-lg form-control-solid" name="teacher_number" value="{{$profile['teacher_number']}}" placeholder="Phone" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Email Address</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="input-group input-group-lg input-group-solid">
                                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-at"></i></span></div>
                                                                <input type="text" class="form-control form-control-lg form-control-solid" value="{{$profile['email']}}" placeholder="Email" disabled />
                                                                <input type="hidden" class="form-control form-control-lg form-control-solid" name="userid" value="{{$profile['id']}}" />
                                                                
                                                            </div>
                                                            <span class="form-text text-muted">Email address cannot be changed</span>
                                                        </div>
                                                    </div>
													<div class="dsepareator">
														<h4>Change Password</h4>
													</div>
													<div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">New password</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <input class="form-control form-control-lg form-control-solid" name="new_pass" type="password" value=""/>
                                                        </div>
                                                    </div>
													<div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Confirm new password</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <input class="form-control form-control-lg form-control-solid" name="confirm_new_pass" type="password" value=""/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end::Body-->
                                            </form>
                                        <!--end::Form-->
                                    </div>
                                <!--end::Card-->
                            @else
                                <div class="card card-custom gutter-b">
                                    <!--begin::Header-->
                                        <div class="card-header py-3">
                                            <div class="align-items-start flex-column">
                                                <h3 class="card-label font-weight-bolder text-dark">Dashboard</h3>
                                                <span class="text-muted font-weight-bold font-size-sm mt-1">Teacher's analytical informaiton</span>
                                            </div>
                                            {{-- <div class="card-toolbar">
                                                <button type="reset" class="btn btn-success mr-2">Save Changes</button>
                                                <button type="reset" class="btn btn-secondary">Cancel</button>
                                            </div> --}}
                                        </div>
                                    <!--end::Header-->

									<!--begin::Body-->
									@if($profile['access_level'] == 2)
										<div class="card-body">
											<!--begin: Items-->
											<div class="d-flex align-items-center flex-wrap">
												<!--begin: Item-->
												<div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
													<span class="mr-4">
														<i class="flaticon-customer icon-2x text-muted font-weight-bold"></i>
													</span>
													<div class="d-flex flex-column text-dark-75">
														<span class="font-weight-bolder font-size-sm">Active Students</span>
														<span class="font-weight-bolder font-size-h5">
														<span class="text-dark-50 font-weight-bold"></span>{{count($active)}}</span>
													</div>
												</div>
												<!--end: Item-->
												<!--begin: Item-->
												<div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
													<span class="mr-4">
														<i class="flaticon-user-add icon-2x text-muted font-weight-bold"></i>
													</span>
													<div class="d-flex flex-column text-dark-75">
														<span class="font-weight-bolder font-size-sm">Student Requests</span>
														<span class="font-weight-bolder font-size-h5">
														<span class="text-dark-50 font-weight-bold"></span>{{count($pending)}}</span>
													</div>
												</div>
												<!--end: Item-->
												<!--begin: Item-->
												{{-- <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
													<span class="mr-4">
														<i class="flaticon-calendar-1 icon-2x text-muted font-weight-bold"></i>
													</span>
													<div class="d-flex flex-column text-dark-75">
														<span class="font-weight-bolder font-size-sm">Monthly Evaluation</span>
														<span class="font-weight-bolder font-size-h5">
														<span class="text-dark-50 font-weight-bold"></span>2</span>
													</div>
												</div> --}}
												<!--end: Item-->
												<!--begin: Item-->
												{{-- <div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
													<span class="mr-4">
														<i class="flaticon-email-black-circular-button icon-2x text-muted font-weight-bold"></i>
													</span>
													<div class="d-flex flex-column flex-lg-fill">
														<span class="text-dark-75 font-weight-bolder font-size-sm">15 Unread Mails</span>
														<a href="#" class="text-primary font-weight-bolder">View</a>
													</div>
												</div> --}}
												<!--end: Item-->
											</div>
											<!--begin: Items-->
										</div>
									@else 
										<div class="card-body">
											<!--begin: Items-->
											<div class="d-flex align-items-center flex-wrap">
												<!--begin: Item-->
												<div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
													<span class="mr-4">
														<i class="flaticon-customer icon-2x text-muted font-weight-bold"></i>
													</span>
													<div class="d-flex flex-column text-dark-75">
														<span class="font-weight-bolder font-size-sm">Active Teachers</span>
														<span class="font-weight-bolder font-size-h5">
														<span class="text-dark-50 font-weight-bold"></span>{{count($teachers)}}</span>
													</div>
												</div>
												<!--end: Item-->
												<!--begin: Item-->
												<div class="d-flex align-items-center flex-lg-fill mr-5 my-1">
													<span class="mr-4">
														<i class="flaticon-user-add icon-2x text-muted font-weight-bold"></i>
													</span>
													<div class="d-flex flex-column flex-lg-fill">
														<a href="/teacher/add" class="btn btn-light-success font-weight-bold mr-2">Add Teacher</a>
													</div>
												</div>
												<!--end: Item-->
											</div>
											<!--begin: Items-->
										</div>
									@endif
									<!--end::Body-->
                                </div>
                                <!--begin::Content-->
									@if($profile['access_level'] == 2)
										<div class="flex-row-lg-fluid">
											<!--begin::Row-->
											<div class="row">

												@foreach($active as $active_students)
													<!--begin::Col-->
													<div class="col-xl-4">
														<!--begin::Card-->
														<div class="card card-custom gutter-b card-stretch">
															<!--begin::Body-->
															<div class="card-body pt-4 d-flex flex-column justify-content-between">
																<!--begin::User-->
																<div class="d-flex align-items-center mb-7">
																	<!--begin::Pic-->
																	<div class="flex-shrink-0 mr-4 mt-lg-0 mt-3">
																		<div class="symbol symbol-lg-75 symbol-success">
																			<span class="symbol-label font-size-h3 font-weight-boldest">{{$active_students['abrivation']}}</span>
																		</div>
																	</div>
																	<!--end::Pic-->
																	<!--begin::Title-->
																	<div class="d-flex flex-column">
																		<a href="/students/profile/{{$active_students['user_id']}}?tab=overview" class="text-dark font-weight-bold text-hover-success font-size-h4 mb-0">{{$active_students['middle_name']}}</a>
																		<span class="text-muted font-weight-bold">{{$active_students['status']}}</span>
																	</div>
																	<!--end::Title-->
																</div>
																<!--end::User-->
																<!--begin::Info-->
																<div class="mb-7">
																	<div class="d-flex justify-content-between align-items-center">
																		<span class="text-dark-75 font-weight-bolder mr-2">Email:</span>
																		<a href="#" class="text-muted text-hover-success">{{$active_students['email']}}</a>
																	</div>
																	<div class="d-flex justify-content-between align-items-cente my-2">
																		<span class="text-dark-75 font-weight-bolder mr-2">Phone:</span>
																		<a href="#" class="text-muted text-hover-success">{{$active_students['contact_number']}}</a>
																	</div>
																	<div class="d-flex justify-content-between align-items-center">
																		<span class="text-dark-75 font-weight-bolder mr-2">School:</span>
																		<span class="text-muted font-weight-bold">{{$active_students['school']}}</span>
																	</div>
																</div>
																<!--end::Info-->
																<a href="/students/profile/{{$active_students['user_id']}}?tab=overview" class="btn btn-block btn-sm btn-light-success font-weight-bolder text-uppercase py-4">View Profile</a>
															</div>
															<!--end::Body-->
														</div>
														<!--end::Card-->
													</div>
													<!--end::Col-->
												@endforeach
											</div>
											<!--end::Row-->
										</div>
									@else
										<div class="flex-row-lg-fluid">
											<!--begin::Row-->
											<div class="row">

												@foreach($teachers as $active_teacher)
													<!--begin::Col-->
													<div class="col-xl-4">
														<!--begin::Card-->
														<div class="card card-custom gutter-b card-stretch">
															<!--begin::Body-->
															<div class="card-body pt-4 d-flex flex-column justify-content-between">
																<!--begin::User-->
																<div class="d-flex align-items-center mb-7">
																	<!--begin::Pic-->
																	<div class="flex-shrink-0 mr-4 mt-lg-0 mt-3">
																		<div class="symbol symbol-lg-75 symbol-success">
																			<span class="symbol-label font-size-h3 font-weight-boldest">{{$active_teacher['abrivation']}}</span>
																		</div>
																	</div>
																	<!--end::Pic-->
																	<!--begin::Title-->
																	<div class="d-flex flex-column">
																		<a href="/teacher/profile/{{$active_teacher['id']}}?tab=overview" class="text-dark font-weight-bold text-hover-success font-size-h4 mb-0">{{$active_teacher['first_name']}} {{$active_teacher['last_name']}}</a>
																		<span class="text-muted font-weight-bold"></span>
																	</div>
																	<!--end::Title-->
																</div>
																<!--end::User-->
																<!--begin::Info-->
																<div class="mb-7">
																	<div class="d-flex justify-content-between align-items-center">
																		<span class="text-dark-75 font-weight-bolder mr-2">Email:</span>
																		<a href="#" class="text-muted text-hover-success">{{$active_teacher['email']}}</a>
																	</div>
																	{{-- <div class="d-flex justify-content-between align-items-cente my-2">
																		<span class="text-dark-75 font-weight-bolder mr-2">Phone:</span>
																		<a href="#" class="text-muted text-hover-success"></a>
																	</div>
																	<div class="d-flex justify-content-between align-items-center">
																		<span class="text-dark-75 font-weight-bolder mr-2">School:</span>
																		<span class="text-muted font-weight-bold"></span>
																	</div> --}}
																</div>
																<!--end::Info-->
																<a href="/teacher/profile/{{$active_teacher['id']}}?tab=overview" class="btn btn-block btn-sm btn-light-success font-weight-bolder text-uppercase py-4">View Profile</a>
															</div>
															<!--end::Body-->
														</div>
														<!--end::Card-->
													</div>
													<!--end::Col-->
												@endforeach
											</div>
											<!--end::Row-->
										</div>
									@endif
									<!--end::Content-->
                            @endif
                        </div>
                        
                    </div>
                <!--end::Profile Personal Information-->
            </div>
            <!--end::Container-->
        </div>
    <!--end::Entry-->
@endsection


@section('scripts')
    <script>
		$( document ).ready(function() {
			console.log( "ready!" );

			$("#save_profile_changes").click(function(e){
				e.preventDefault();

				// console.log('submit clicked');
				$("#update_teacher_profile").submit();
			});
		});
	</script>
@endsection
