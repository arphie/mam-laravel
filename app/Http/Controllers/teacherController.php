<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

// initalize services
use App\Http\Services\teachers\TeacherService;

use App\Exports\SentenseExport;
use Maatwebsite\Excel\Facades\Excel;

class teacherController extends Controller
{
    //

    public function profile(
        $id,
        TeacherService $teacher
    )
    {
        $teacher_id = Session::get('user');
        $header_profile = $teacher->getTeacherProfile($teacher_id);
        if($id != $teacher_id && $header_profile['access_level'] != 1){
            return view('405');
        }
        $profile = $teacher->getTeacherProfile($id);
        if($profile['access_level'] == '1'){
            $teachers = $teacher->getAllTeachers();
            return view('teacher_profile')->with(['profile' => $profile, 'teachers' => $teachers, 'active_user' => $header_profile]);
        } else {
            $pending_students = $teacher->getPairedStudents($id, 'pending');
            $active_students = $teacher->getPairedStudents($id, 'active');
            return view('teacher_profile')->with(['profile' => $profile, 'pending' => $pending_students, 'active' => $active_students, 'active_user' => $header_profile]);
        }
        
        
    }

    public function get_teacher_info(
        Request $request,
        TeacherService $teacher
    )
    {
        $data = $request->all();
        $teacher_info = $teacher->getTeacherInfo($data);
        return $teacher_info;
    }

    public function change_password()
    {
        return view('change_pass');
    }

    public function add_teacher(
        TeacherService $teacher
    )
    {
        
        $teacher_id = Session::get('user');
        $profile = $teacher->getTeacherProfile($teacher_id);
        return view('add_teacher')->with(['profile' => $profile, 'active_user' => $profile]);
    }
    

    public function process_add_teacher(
        Request $request,
        TeacherService $teacher
    )
    {
        $data = $request->all();
        $registration = $teacher->register($data);
        return $registration;


        // return redirect('/');

        // exit;
    }
    
    public function saveTeacher(
        Request $request,
        TeacherService $teacher
    )
    {
        $data = $request->all();
        $registration = $teacher->saveTeacher($data);
        return $registration;
    }

    public function insertTeacherInfo(
        Request $request,
        TeacherService $teacher
    )
    {
        $data = $request->all();
        $registration = $teacher->insertTeacherInfo($data);
        return $registration;
    }

    public function studentRequest(
        Request $request,
        TeacherService $teacher
    )
    {
        $data = $request->all();
        $teacher_request = $teacher->sendTeacherRequest($data);
        return $teacher_request;
    }

    public function updateprofile(
        Request $request,
        TeacherService $teacher
    )
    {
        $data = $request->all();
        $teacher_request = $teacher->updateTeacherProfile($data);
        return $teacher_request;
        
    }

    public function testmail(
        TeacherService $teacher
    )
    {
        $teacher->testmail();
    }

    public function export(
        TeacherService $teacher,
        $id,
        $type
    )
    {
        return $teacher->export_data($id, $type);
    }
}