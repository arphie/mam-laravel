<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Response;

use App\Http\Services\teachers\TeacherService;
use App\Http\Services\students\StudentService;

class authController extends Controller
{
    public function login()
    {
        if (Session::has('user')) {
            // user value cannot be found in session
            $session_id = Session::get('user');
            return redirect('/teacher/profile/'.$session_id.'?tab=overview');
        }
        return view('login');
    }

    public function register()
    {
        return view('register');
    }

    public function download()
    {
        return view('download');
    }

    public function process_registration(
        Request $request
    )
    {
        $data = $request->all();

        print_r($data);
        exit;
    }

    public function processlogin(
        Request $request,
        TeacherService $teacher
    )
    {
        $data = $request->all();
        $login_info = $teacher->loginTeacher($data);
        return $login_info;
    }

    public function processloginapi(
        Request $request,
        TeacherService $teacher,
        StudentService $student
    )
    {
        $data = $request->all();
        $login_info = $student->loginStudent($data);
        return $login_info;
    }

    public function logout()
    {
        Session::flush();
        return redirect('/login');
    }

    public function userNotAllowed()
    {
        return view('405');
    }

    public function forgotPass(
        Request $request,
        TeacherService $teacher
    )
    {
        $data = $request->all();
        $login_info = $teacher->forgotPass($data);
        return $login_info;
    }

    public function downloadapk()
    {
        // dump('this is a rtest');
        $filepath = public_path('mam.apk');
        return Response::download($filepath); 
    }

    public function downloadios()
    {
        // dump('this is a rtest');
        $filepath = public_path('mam.m');
        return Response::download($filepath); 
    }
}
