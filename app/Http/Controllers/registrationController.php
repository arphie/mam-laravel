<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Services\registration\userRegistrationService;

class registrationController extends Controller
{
    //

    public function injectUser(
        Request $request,
        userRegistrationService $userRegistration
    )
    {
        $data = $request->all();
        $save_student = $userRegistration->register($data);
        return $save_student;
    }
}
