<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

use App\Http\Services\students\StudentService;
use App\Http\Services\teachers\TeacherService;

class studentsController extends Controller
{
    //

    public function view_page()
    {
        return view('students');
    }

    public function profile(
        $id,
        StudentService $student,
        TeacherService $teacher
    )
    {
        $teacher_id = Session::get('user');
        $header_profile = $teacher->getTeacherProfile($teacher_id);
        $profile = $teacher->getTeacherProfile($teacher_id);
        $student_information = $student->get_student_data($id);
        $teacher_information = $teacher->get_teacher_details($id);
        $board_information = $student->get_students_boards($id);
        $topTiles = $student->get_top_tiles($id);
        $tileStats = $student->tileStats($id);
        $sentenses = $student->get_sentenses($id);
        $notes = $student->get_notes($id);
        // exit;
        return view('student_profile')->with(['profile' => $profile, "student_info"=> $student_information, 'teacher_info' => $teacher_information, 'board_info' => $board_information, 'active_user' => $header_profile, 'tile_stats' => $tileStats, 'sentenses' => $sentenses, 'notes' => $notes]);
    }

    public function deletetile(
        $id,
        $tileid,
        StudentService $student
    )
    {
        $deleteItrem = $student->deleteTile($tileid);
        return redirect('/students/profile/'.$id.'?tab=board');
    }

    public function students_request()
    {
        return view('students_request');
    }

    /**
     * API: save student
     */

    public function saveStudent(
        Request $request,
        StudentService $student
    )
    {
        $data = $request->all();
        $student_info = $student->saveStudent($data);
        return $student_info;
    }

    public function approveStudent(
        $id,
        StudentService $student
    )
    {
        $student->approveStudentRequest($id);
        return redirect('/students/profile/'.$id.'?tab=overview');
    }

    public function introduce_tile(
        $id,
        StudentService $student,
        TeacherService $teacher
    )
    {
        $teacher_id = Session::get('user');
        $header_profile = $teacher->getTeacherProfile($teacher_id);
        $profile = $teacher->getTeacherProfile($teacher_id);
        $student_information = $student->get_student_data($id);
        $teacher_information = $teacher->get_teacher_details($id);
        $tiles = $student->get_all_tiles($id);
        return view('student_tiles')->with(['profile' => $profile, "student_info"=> $student_information, 'teacher_info' => $teacher_information, 'active_user' => $header_profile, 'tiles' => $tiles]);
    }

    public function process_introduce_tile(
        $id,
        StudentService $student,
        TeacherService $teacher,
        Request $request
    )
    {
        $data = $request;
        $saved_tile = $student->save_tile($data);
        return $saved_tile;
    }

    public function trackuseractivity(
        Request $request,
        StudentService $student
    )
    {
        $data = $request->all();
        $saved_tile = $student->logtile($data);
        return $saved_tile;
    }

    public function getPecsImages(
        Request $request,
        StudentService $student
    )
    {
        $data = $request->all();
        $images = $student->getPecsImages($data['user_id']);
    }

    public function get_student_pecs(
        $id,
        StudentService $student
    )
    {
        $student_info = $student->getStudentPecs($id);
        return $student_info;
    }

    public function get_student_pecs_images(
        $id,
        StudentService $student
    )
    {
        $student_info = $student->getStudentPecsImages($id);
        return $student_info;
    }

    public function updateStudentInfo(
        Request $request,
        StudentService $student
    )
    {
        $data = $request->all();
        $student_info = $student->updateStudentInfo($data);
        return $student_info;
    }

    public function getStudentInfo(
        $id,
        StudentService $student
    )
    {
        $student_info = $student->getStudentInfo($id);
        return $student_info;
    }

    public function saveCorrectSentense(
        $id,
        $text,
        StudentService $student
    )
    {
        $saved_sennse = $student->save_student_information($id, $text);
        return redirect('/students/profile/'.$id.'?tab=sentenses');
    }

    public function add_student_notes(
        Request $request,
        StudentService $student
    )
    {
        $data = $request->all();
        $student_info = $student->add_student_notes($data);
        return redirect('/students/profile/'.$data['student_id'].'?tab=overview');
    }

}
