<?php


namespace App\Http\Repositories;

use App\Http\Repositories\BaseRepository;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTime;
use Session;
Use Exception;
use Illuminate\Support\Facades\Mail;



// list of models
use App\Models\User;
use App\Models\UserInfoModel;
use App\Models\PairedStudentsModel;
use App\Models\StudentInfoModel;

class TeachersRepository extends BaseRepository
{
    /**
     * Declaration of Variables
     */
    private $user;
    private $user_info;
    

    /**
     * PropertyRepository constructor.
     * @param Fund 
     */
    public function __construct(
        User $usersModel,
        UserInfoModel $userInfoModel,
        StudentInfoModel $studentInfoModel,
        PairedStudentsModel $pairedModel
    ){
        $this->user = $usersModel;
        $this->user_info = $userInfoModel;
        $this->student_info = $studentInfoModel;
        $this->paired = $pairedModel;
    }

    // public function getModel($model)
    // {
    //     if($model == 'PermissionsModel'){
    //         return $this->permissions; // select pixel model
    //     } else {
    //         return false;
    //     }
    // }

    public function pass_hash($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function register($data)
    {
        // save user account
        $generatedpass = $this->pass_hash();
        
        $user_save = [];
        $user_save['first_name'] = $data['firstname'];
        $user_save['last_name'] = $data['lastname'];
        $user_save['middle_name'] = $data['middlename'];
        $user_save['email'] = $data['email'];
        $user_save['password'] = $generatedpass;

        // $register = $this->registerItem($this->user, $user_save);
        try{
            $register = $this->user->insertGetId($user_save);
        }
        catch(Exception $e)
        {
            return 0;
            dd($e->getMessage());
        }
        

        $user_id = $register;
        $data['dteachertoken'] = $this->generateRandomString(8);
        // save user info
        $filter_keys = ['firstname', 'lastname', 'middlename', 'email', '_token'];
        $user_info = array_diff_key($data, array_flip($filter_keys));
        foreach ($user_info as $key => $value) {
            $info = [];
            $info['user_id'] = $user_id;
            $info['info_name'] = $key;
            $info['info_value'] = $value;
            $infobase = $this->registerItem($this->user_info, $info);
        } 

        // send email
        $to_name = $data['firstname']." ".$data['lastname'];
        $to_email = $data['email'];
        $data = [
            'name'=> $data['firstname'],
            'email' => $data['email'],
            'pass' => $generatedpass,
        ];
        Mail::send('emails.registration', $data, function($message) use ($to_name, $to_email) {
        $message->to($to_email, $to_name)->subject('Pecs Board Invitation');

        $message->from('e3arphie@gmail.com', 'Pecs Board Invitation');
        });

        return $register;
    }

    public function saveTeacherAccount($data)
    {
        
        $data['password'] = md5($data['password']);
        $register = $this->registerItem($this->user, $data);
        return $register;
    }

    public function update($data)
    {
        if(!isset($data['id']) || $data['id'] == ""){
            return [
                'status' => 500,
                'message' => 'ID is required.',
                'data' => [],
            ];
        }

        $model = $this->getModel($data['model']);

        if(!$model){
            return [
                'status' => 500,
                'message' => 'Model not found',
                'data' => '',
            ];
        }

        $update = $this->updateItem($model, $data);

        return $update;
    }


    public function delete($data)
    {
        $model = $this->getModel($data['model']);

        if(!$model){
            return [
                'status' => 500,
                'message' => 'Model not found',
                'data' => '',
            ];
        }

        $delete = $this->deleteItem($model, $data['id']);
        return $delete;
    }

    public function sendStudentRequest($data)
    {
        $student_info = $data;
        unset($student_info['teacher']);
        $userinfoid = $this->student_info->insertGetId($student_info);
        
        $teacher_request = [
            "user_id" => $userinfoid,
            "teacher_id" => $data['teacher'],
            "status" => 'pending',
        ];
        $this->paired->insert($teacher_request);
        return [
            'status' => 200,
            'message' => 'Request sent',
            'data' => $userinfoid,
        ];
    }

    public function getTeacherProfile($id)
    {
        $teacherinfo = $this->returnToArray($this->user->where('id', '=', $id)->first());

        // get information
        $teacher_information = $this->returnToArray($this->user_info->where('user_id', '=', $id)->get());
        foreach ($teacher_information as $tkey => $tivalue) {
            $teacherinfo[$tivalue['info_name']] = $tivalue['info_value'];
        }

        if(!isset($teacherinfo['profile_avatar'])){
            $teacherinfo['profile_avatar'] = 'media/users/default.jpg';
        }
        // dump($teacherinfo);
        // dump($teacher_information);
        return $teacherinfo;
    }

    public function getPairedStudents($id, $type)
    {
        $student_values = [];
        $paired_students = $this->returnToArray($this->paired->where([['teacher_id', '=', $id],['status', '=', $type]])->get());
        foreach ($paired_students as $pskey => $psvalue) {
            $dbase_info = $this->returnToArray($this->student_info->where('id', '=', $psvalue['user_id'])->first());
            if(!empty($dbase_info)){
                // create abrivation
                $dbase_info['abrivation'] = strtoupper($dbase_info['first_name'][0] . $dbase_info['last_name'][0]);
                $dbase_info['user_id'] = $psvalue['user_id'];
                $dbase_info['status'] = $psvalue['status'];
                $dbase_info['teacher_id'] = $psvalue['teacher_id'];
                unset($dbase_info['password']);
                unset($dbase_info['deleted_at']);
                unset($dbase_info['created_at']);
                unset($dbase_info['updated_at']);
                array_push($student_values,$dbase_info);
            }
        }
        return $student_values;
    }

    public function getTeacherDetails($id)
    {
        $teacher_status = $this->returnToArray($this->paired->where('user_id', '=', $id)->first());
        $teacher_info = $this->returnToArray($this->user->where('id', '=', $teacher_status['teacher_id'])->first());
        if(!empty($teacher_info)){
            $teacher_info['user_id'] = $teacher_status['user_id'];
            $teacher_info['teacher_id'] = $teacher_status['teacher_id'];
            $teacher_info['status'] = $teacher_status['status'];
        }
        return $teacher_info;
    }

    public function approveStudentRequest($id)
    {
        $this->paired->where('user_id', '=', $id)->update(['status' => 'active']);
    }

    public function getAllTeachers()
    {
        $teachers = [];
        $teacher_status = $this->returnToArray($this->user->where('access_level', '=', 2)->get());
        if(!empty($teacher_status)){
            foreach ($teacher_status as $pskey => $psvalue) {
                // get user data
                $infobase = $this->returnToArray($this->user_info->where('user_id', '=', $psvalue['id'])->get());
                $psvalue['information'] = $infobase; 
                // create abrivation
                $psvalue['abrivation'] = strtoupper($psvalue['first_name'][0] . $psvalue['last_name'][0]);
                unset($psvalue['password']);
                unset($psvalue['deleted_at']);
                unset($psvalue['created_at']);
                unset($psvalue['updated_at']);
                array_push($teachers,$psvalue);
            }
        }
        return $teachers;
    }

    public function loginTeacher($data)
    {
        $teacher_status = $this->returnToArray($this->user->where([['email', '=', $data['username']], ['password', '=', $data['password']]])->first());
        if(!empty($teacher_status)){

            Session::put('user', $teacher_status['id']);

            return redirect('/teacher/profile/'.$teacher_status['id'].'?tab=overview');
        } else {
            // return [
            //     'status' => 500,
            //     'message' => 'Login Error',
            //     'data' => '',
            // ];
            return view('/login')->with(['serrors' => 'credentials did not match']);
        }
    }

    public function updateTeacherProfile($data)
    {
        if($data['new_pass'] != ""){
            if($data['new_pass'] != $data['confirm_new_pass']){
                return redirect('/teacher/profile/'.$data['userid'].'?tab=personal&error=one');
            }

            $this->user->where('id', '=', $data['userid'])->update([
                'password' => $data['new_pass']
            ]);
        }
        
        /**
         * save basic info
         */

        $this->user->where('id', '=', $data['userid'])->update([
            'first_name' => $data['firstname'],
            'last_name' => $data['lastname'],
            'middle_name' => $data['middlename']
        ]);
        
        
        /**
         * save teacher number
         */
        $this->user_info->updateOrCreate([
            'user_id' => $data['userid'],
            'info_name' => 'teacher_number'
        ], [
            'info_value' => $data['teacher_number']
        ]);

        /**
         * save teacher profile
         */

        $save_to_path = 'media/profiles';
        $filetosave = '';

        if ($file = $data['profile_avatar']) {
            $destination = public_path($save_to_path);
            $name = $file->getClientOriginalName();
            $file->move($destination, $name);
            $filetosave = "media/profiles/".$name;
            
            $this->user_info->updateOrCreate([
                'user_id' => $data['userid'],
                'info_name' => 'profile_avatar'
            ], [
                'info_value' => $filetosave
            ]);
        }

        return redirect('/teacher/profile/'.$data['userid'].'?tab=personal&success=one');
    }

    public function getTeacherInfo($data)
    {
        $user_info_data_id = $this->returnToArray($this->user_info->where('info_value', '=', $data['teacher'])->first());

        if(empty($user_info_data_id)){
            return [
                'status' => 500,
                'message' => 'Teacher info fetched',
                'data' => [],
            ];
        }

        $information = $this->returnToArray($this->user->where('id', '=', $user_info_data_id['user_id'])->first());
        $alinfo = $this->returnToArray($this->user_info->where('user_id', '=', $user_info_data_id['user_id'])->get());

        foreach ($alinfo as $key => $value) {
            if($value['info_name'] == 'profile_avatar'){
                $value['info_value'] = 'http://pecsboard.com/'.$value['info_value'];
            }
            $information[$value['info_name']] = $value['info_value'];
        }
        

        return [
            'status' => 200,
            'message' => 'Teacher info fetched',
            'data' => $information,
        ];
    }

    public function testmail()
    {
        dump('test mail');

        $to_name = 'sample persoin';
        $to_email = 'kennethbalboa@cyberbacker.com';
        $data = [
            'name'=> 'smaple name'
        ];
        Mail::send('emails.registration', $data, function($message) use ($to_name, $to_email) {
        $message->to($to_email, $to_name)->subject('Laravel Test Mail');

        $message->from('e3arphie@gmail.com', 'Pecs Board Invitation');
        });

        exit;
    }

    public function forgotPass($data)
    {
        $information = $this->returnToArray($this->user->where('email', '=', $data['email'])->first());
        if(!empty($information)){
            $password = $this->pass_hash();
            $this->user->where('id', '=', $data['userid'])->update([
                'password' => $password
            ]);

            $to_name = $information['first_name']." ".$information['last_name'];
            $to_email = $data['email'];
            $data = [
                'name' => $information['first_name'],
                'pass' => $password
            ];
            Mail::send('emails.forgot', $data, function($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)->subject('Forgot Password Confirmation');

                $message->from('e3arphie@gmail.com', 'Forgot Password Confirmation');
            });
            return redirect('/login?stats=success'); 
        }

        return redirect('/login?stats=fail'); 

        exit;
    }

    public function get_student_sentenses($id)
    {
        dump('get student senternses');

        $sentenses = $this->returnToArray($this->user->where('email', '=', $data['email'])->first());
    }
    
}
