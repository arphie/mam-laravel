<?php


namespace App\Http\Repositories;

use App\Http\Repositories\BaseRepository;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTime;

// list of models
use App\Models\StudentInfoModel;
use App\Models\PecsModel;
use App\Models\PecsLogModel;
use App\Models\OrderModel;
use App\Models\CorrectSentenseModel;
use App\Models\StudentsNotesModel;

class StudentsRepository extends BaseRepository
{
    /**
     * Declaration of Variables
     */
    private $user;
    

    /**
     * PropertyRepository constructor.
     * @param Fund 
     */
    public function __construct(
        StudentInfoModel $usersModel,
        PecsModel $pecsModel,
        PecsLogModel $pecsLogModel,
        OrderModel $orderLogModel,
        CorrectSentenseModel $correctSentenseModel,
        StudentsNotesModel $studentNotes
    ){
        $this->user = $usersModel;
        $this->pecs = $pecsModel;
        $this->logs = $pecsLogModel;
        $this->order = $orderLogModel;
        $this->correct_sentense = $correctSentenseModel;
        $this->notes = $studentNotes;
    }


    public function register($data)
    {

        $register = $this->registerItem($this->user, $data);

        return $register;
    }

    public function update($data)
    {
        
    }


    public function delete($data)
    {

    }

    public function getStudentInformation($id)
    {
        
        $userinfo = $this->returnToArray($this->user->where('id', '=', $id)->first());
        $userinfo['birth_date'] = Carbon::parse($userinfo['birth_date'])->format('F d, Y');
        return $userinfo;
    }

    public function save_tile($data)
    {
        $newdata = $data->all();
        $file = $data->file('newtile');

        $linkbasename = "";
        if($file = $data->file('newtile')){
            $destination = public_path('media/tiles');
            $current_date_time = Carbon::now()->timestamp;
            $basename = $file->getClientOriginalName();
            $linkbasename = "https://".$_SERVER['SERVER_NAME'].'/media/tiles/'.$file->getClientOriginalName();
            
            $file->move($destination, $linkbasename);
        }
        $id = $this->pecs->insertGetId([
            'user_id' => $data['userid'],
            'name' => $data['name'],
            'category' => $data['category'],
            'tile' => ($linkbasename != "" ? $linkbasename : $data['tile']),
        ]);

        $this->order->insert([
            "tile_id" => $id,
            "order_id" => $data['order']
        ]);


        return redirect('/students/profile/'.$data['userid'].'?tab=board');
    }

    public function getboarditems($id)
    {
        // get items
        $items = $this->returnToArray($this->pecs->where('user_id', '=', $id)->get());

        // get categories
        $tiles = [];
        $categories = [];
        foreach ($items as $catkey => $catvalue) {
            $categories[] = $catvalue['category'];
        }
        $categories = array_unique($categories);

        foreach ($categories as $dcatkey => $dcatvalue) {
            $tile_cat_base = [];
            $tile_cat_base['name'] = $dcatvalue;
            foreach ($items as $itemskey => $itemsvalue) {
                if($itemsvalue['category'] == $dcatvalue){
                    $tile_cat_base['items'][] = $itemsvalue;
                }
            }
            array_push($tiles, $tile_cat_base);
        }
        return $tiles;
    }

    public function logtile($data)
    {
        foreach ($data['entry'] as $key => $value) {
            unset($value["_id"]);
            $value['created_at'] = Carbon::now();
            $this->logs->insertGetId($value);
        }

        return [
            'status' => 200,
            'message' => 'Logs saved',
            'data' => '',
        ];
    }

    public function gitMedia($id)
    {
        $getImages = $this->returnToArray($this->pecs->where('user_id', '=', $id)->get('tile'));
        dump($getImages);

        

    }

    public function getPecsByStudentID($id)
    {
        $getImages = $this->returnToArray($this->pecs->where('user_id', '=', $id)->get(['id', 'name', 'category', 'tile']));

        // filter by category
        $category = [];
        foreach ($getImages as $catkey => $catvalue) {
            $category[] = $catvalue['category'];
        }
        $category = array_unique($category);

        $final = [];
        foreach ($category as $cskey => $csvalue) {
            $dpecs = [];
            $dpecs['heading'] = $csvalue;
            $dpecs['id'] = $cskey;
            $dpecs['icons'] = [];
            foreach ($getImages as $xkey => $xvalue) {
                if($csvalue == $xvalue['category']){
                    $paced = [];

                    $paced['name'] = $xvalue['name'];
                    $paced['icon'] = $xvalue['tile'];
                    $paced['word'] = $xvalue['name'];
                    $paced['id'] = $xkey;

                    $get_order = $this->returnToArray($this->order->where('tile_id', '=', $xvalue['id'])->first(['order_id']));
                    if(!empty($get_order)){
                        $paced['order'] = $get_order['order_id'];
                    } else {
                        $paced['order'] = 0;
                    }


                    $dpecs['image'] = $xvalue['tile'];
                    array_push($dpecs['icons'], $paced);
                }
            }
            array_push($final, $dpecs);
        }

        return [
            'status' => 200,
            'message' => 'student pecs board',
            'data' => $final,
        ];
    }

    public function deleteTile($tileid)
    {
        $this->pecs->where('id', '=', $tileid)->delete();
    }

    public function get_top_tiles($id)
    {
        $allTiles = $this->returnToArray($this->logs->where('student_id', '=', $id)->get());
        // get tiles
        $tiles = [];
        foreach ($allTiles as $tileskey => $tilesvalue) {
            $tiles[] = $tilesvalue['name'];
        }

        $newTiles = array_unique($tiles);
        $counter = [];
        foreach ($newTiles as $cckey => $ccvalue) {
            // $counter[$ccvalue] = 0;
            $counter[$ccvalue] = count( preg_grep( "/^".$ccvalue."/", $tiles ) );
        }
        arsort($counter);
        // dump($counter);

        return $counter;
    }

    public function tileStats($id)
    {
        $allTiles = $this->returnToArray($this->logs->where('student_id', '=', $id)->get());

        // get tiles
        $attempts = 0;
        $correct = 0;
        $tiles = [];
        $traces = [];
        $sentenses = [];
        foreach ($allTiles as $tileskey => $tilesvalue) {
            // $attempts++;
            // if($tilesvalue['iscorrect'] == 'yes'){
            //     $correct++;
            // }
            $sentenses[] = $tilesvalue['sentense'];
            $tiles[$tileskey]['name'] = $tilesvalue['name'];
            $tiles[$tileskey]['tile'] = $tilesvalue['tile'];
            $traces[] = $tilesvalue['septrace']."-".$tilesvalue['iscorrect'];
        }
        // 
        $newsentenses = array_unique($sentenses);
        $newtrace = array_unique($traces);
        // dump($newtrace);
        $newsentenses = array_reverse($newsentenses);

        $sentensewithdate = [];
        foreach ($newsentenses as $senkey => $senvalue) {
            $sentensewithdate[$senkey]['sentense'] = $senvalue;
            foreach ($allTiles as $atkey => $atvalue) {
                
                if($senvalue == $atvalue['sentense']){
                    // dump($atvalue['created_at']);
                    // dump(Carbon::parse($atvalue['created_at'])->format('D'));
                    $sentensewithdate[$senkey]['created_at'] = Carbon::parse($atvalue['created_at'])->format('F d, Y g:iA');
                    $sentensewithdate[$senkey]['created_at_raw'] = Carbon::parse($atvalue['created_at'])->format('D');
                }
                // dump($atvalue);
            }
        }


        // get number of correct
        
        $septraces = [];
        foreach ($allTiles as $key => $value) {
            
            $septraces[$value['septrace']]['name'] = $value['sentense'];

            $createdAt = Carbon::parse($value['created_at']);

            $septraces[$value['septrace']]['created'] = $createdAt->format('M d, Y g:iA');
            $septraces[$value['septrace']]['is_day'] = $createdAt->format('D');
        }

        // get all correct sentenses
        $corsentenses = $this->returnToArray($this->correct_sentense->get('sentense'));

        // get correct sentenses
        $corsecns = [];
        foreach ($corsentenses as $corskey => $corsvalue) {
            array_push($corsecns, $corsvalue['sentense']);
        }

        $correct_date_count = [
            "Mon" => 0,
            "Tue" => 0,
            "Wed" => 0,
            "Thu" => 0,
            "Fri" => 0,
            "Sat" => 0,
            "Sun" => 0,
        ];

        foreach ($septraces as $sptkey => $sptvalue) {
            $attempts++;
            if(in_array($sptvalue['name'], $corsecns)){
                $correct++;
                $correct_date_count[$sptvalue['is_day']]++;
            } 
        }



        // foreach ($newtrace as $trkey => $trvalue) {
        //     $attempts++;
        //     if(strpos($trvalue, '-yes') !== false){
        //         $correct++;
        //     }
        // }
        

        // $newTiles = array_unique($tiles);
        

        //get all tiles must unique
        $utiles = [];
        foreach ($tiles as $sdkey => $sdvalue) {
            $utiles[] = $sdvalue['name'];
        }
        
        $newTiles = array_unique($utiles); 
        
        
        $counter = [];
        foreach ($newTiles as $cckey => $ccvalue) {
            // $counter[$ccvalue] = 0;
            $counter[$ccvalue] = count( preg_grep( "/^".$ccvalue."/", $utiles ) );
        }
        arsort($counter);
        $counter = array_slice($counter, 0, 5, true);

        $tilewithimage = [];
        foreach ($counter as $key => $value) {
            $ditembase = [];
            $ditembase['name'] = $key;
            $ditembase['count'] = $value;
            foreach ($tiles as $zxkey => $zxvalue) {
                if($key == $zxvalue['name']){
                    $ditembase['tile'] = $zxvalue['tile'];
                }
            }
            array_push($tilewithimage,$ditembase);
        }

        $tiles_count = $this->pecs->where('user_id', '=', $id)->get()->count();
        // get active tiles
        
        $success_rate = 0;
        if($attempts != 0){
            $success_rate = ($correct / $attempts) * 100;
        }

        $success_rate = number_format((float)$success_rate, 3, '.', '');
        
        

        // get sentense as per date
        $date_count = [
            "Mon" => 0,
            "Tue" => 0,
            "Wed" => 0,
            "Thu" => 0,
            "Fri" => 0,
            "Sat" => 0,
            "Sun" => 0,
        ];
        foreach ($sentensewithdate as $nskey => $nsvalue) {
            $date_count[$nsvalue['created_at_raw']]++;
        }

        $sentensewithdate = array_slice($sentensewithdate, 0, 7, true);

        // get max
        $max_date_count = max($date_count) + 1;
        $new_per_day = []; 
        foreach ($date_count as $dckey => $dcvalue) {
            if($dcvalue > 0){
                $newvals = ($dcvalue / $max_date_count) * 100;
                $new_per_day[$dckey]['svals'] = $newvals;
                $new_per_day[$dckey]['dcount'] = $dcvalue;
                continue;
            }
            $new_per_day[$dckey]['svals'] = $dcvalue;
            $new_per_day[$dckey]['dcount'] = $dcvalue;
        }

        $max_date_count_sentense = max($correct_date_count) + 1;
        
        $new_per_day_sentense = []; 
        foreach ($correct_date_count as $sdckey => $sdcvalue) {
            if($sdcvalue > 0){
                $newvals = ($sdcvalue / $max_date_count_sentense) * 100;
                $new_per_day_sentense[$sdckey]['svals'] = $newvals;
                $new_per_day_sentense[$sdckey]['dcount'] = $sdcvalue;
                continue;
            }
            $new_per_day_sentense[$sdckey]['svals'] = $sdcvalue;
            $new_per_day_sentense[$sdckey]['dcount'] = $sdcvalue;
        }
        
        // dump($new_per_day);
        // dump($new_per_day_sentense);
        // dump($date_count);
        // dump($new_per_day);


        $stats = [
            'attempts' => $attempts,
            'correct' => $correct,
            'success' => $success_rate,
            'top_tiles' => $tilewithimage,
            'tiles_count' => $tiles_count,
            'sentenses' => $sentensewithdate,
            'per_day' => $new_per_day,   
            'correct_day' => $new_per_day_sentense,   
        ];
        // dump($stats);
        return $stats;
    }

    public function loginStudent($data)
    {
        $student_info = $this->returnToArray($this->user->where([['email', '=', $data['email']], ['password', '=', $data['password']]])->first());
        
        if(!empty($student_info)){
            return [
                'status' => 200,
                'message' => 'User Auth',
                'data' => $student_info,
            ];
        } else {
            return [
                'status' => 200,
                'message' => 'User Not Found',
                'data' => [],
            ];
        }
    }

    public function studentPecsImages($id)
    {
        $getImages = $this->returnToArray($this->pecs->where('user_id', '=', $id)->get(['name', 'category', 'tile']));

        $final_tiles = [];
        foreach ($getImages as $zkey => $zvalue) {
            array_push($final_tiles, $zvalue['tile']);
        }

        return [
            'status' => 200,
            'message' => 'student pecs images',
            'data' => $final_tiles,
        ];
    }

    public function updateStudentInfo($data)
    {
        $student_id = $data['student'];
        unset($data['student']);

        $this->user->where('id', '=', $student_id)->update($data);

        $student_info = $this->returnToArray($this->user->where('id', '=', $student_id)->first());
        return [
            'status' => 200,
            'message' => 'student info fetched',
            'data' => $student_info,
        ];
    }

    public function getStudentInfo($id)
    {
        $student_info = $this->returnToArray($this->user->where('id', '=', $id)->first());
        return [
            'status' => 200,
            'message' => 'student info fetched',
            'data' => $student_info,
        ];
    }
    
    public function getSentenses($id)
    {
        $student_info = $this->returnToArray($this->logs->where('student_id', '=', $id)->get());

        $sentenses = [];
        $septraces = [];
        foreach ($student_info as $key => $value) {
            $septraces[$value['septrace']]['name'] = $value['sentense'];

            $createdAt = Carbon::parse($value['created_at']);

            $septraces[$value['septrace']]['created'] = $createdAt->format('M d, Y g:iA');
            array_push($sentenses, $value['sentense']);
        }
        $fixed_senteses = array_unique($sentenses);

        // get all correct sentenses
        $corsentenses = $this->returnToArray($this->correct_sentense->get('sentense'));

        // get correct sentenses
        $corsecns = [];
        foreach ($corsentenses as $corskey => $corsvalue) {
            array_push($corsecns, $corsvalue['sentense']);
        }

        
        // comparisonbase 
        $final_sentense = [];
        foreach ($septraces as $sptkey => $sptvalue) {
            $d_sentense_info = $sptvalue;
            if(in_array($sptvalue['name'], $corsecns)){
                $d_sentense_info['iscorrect'] = 'correct';
            } else {
                $d_sentense_info['iscorrect'] = 'incorrect';
            }
            array_push($final_sentense, $d_sentense_info);
        }
        return $final_sentense;
    }

    public function get_notes($id)
    {
        $student_notes = $this->returnToArray($this->notes->where('student_id', '=', $id)->orderBy('id', 'DESC')->get());
        foreach($student_notes as $key => $value){
            $student_notes[$key]['created_at'] = Carbon::parse($student_notes[$key]['created_at'])->format('F d,Y');
        }
        return $student_notes;
    }

    public function save_sentense_per_student($id, $text)
    {
        $this->correct_sentense->insert([
            'sentense' => $text,
            'approved_by' => $id,
            'created_at' => Carbon::now()
        ]);

        return true;
    }

    public function add_student_notes($data)
    {
        $this->notes->insert([
            'student_id' => $data['student_id'],
            'snotes' => $data['d-student-note'],
            'created_at' => Carbon::now()
        ]);
        return true;
    }

    public function export_student_sentenses($id)
    {
        $student_info = $this->returnToArray($this->logs->where('student_id', '=', $id)->get());
        

        // get list of sentenses
        $list_of_sentense = [];
        foreach ($student_info as $key => $value) {
            $list_of_sentense[$value['septrace']]['sentense'] = $value['sentense'];
            $list_of_sentense[$value['septrace']]['created_at'] = Carbon::parse($value['created_at'])->format('M d,Y g:iA');
        }

        // sanitize data
        $to_export = [];
        foreach ($list_of_sentense as $senskey => $sensvalue) {
            $export_arrangement = [];
            $export_arrangement[0] = $sensvalue['sentense'];
            $export_arrangement[1] = $sensvalue['created_at'];
            array_push($to_export, $export_arrangement);
        }

        dump($to_export);



        exit;
    }
}
