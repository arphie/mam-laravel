<?php

namespace App\Http\Services\students;

use App\Http\Services\BaseService;
use Illuminate\Support\Facades\File;


// list of repository
use App\Http\Repositories\TeachersRepository;
use App\Http\Repositories\StudentsRepository;

class StudentService extends BaseService
{   
    private $teacher;
    // private $model;

    public function __construct(
        TeachersRepository $teachersRepo,
        StudentsRepository $StudentsRepo 
    ){
        $this->teacher = $teachersRepo;
        $this->students = $StudentsRepo;
        // $this->model = 'LandingPagesModel';
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function saveStudent($data)
    {
        $userinfo = $this->students->register($data);
        return $this->absorb($userinfo);
    }

    public function get_student_data($id)
    {
        $student_info = [
            'id' => 1,
            'name' => 'John Peter Doe',
            'address' => 'sample address',
            'birthdate' => 'january 01, 1943',
            'mobile' => '2354234234',
            'email' => 'john@doe.com',
            'incase_name' => 'sample incase name',
            'incase_number' => '0345045934503',
            'incase_relationship' => 'mother'
        ];
        $student = $this->students->getStudentInformation($id);
        return $student;
    }
    
    public function get_students_boards($student_id)
    {
        $board = $this->students->getboarditems($student_id);

        return $board;
    }

    public function approveStudentRequest($id)
    {
        $this->teacher->approveStudentRequest($id);
    }

    public function get_all_tiles()
    {
        $all_images = [];
        $path = public_path('media/tiles');
        $files = File::allFiles($path);

        foreach ($files as $key => $value) {
            // dump($value->getRelativePathname());
            $all_images[] = "media/tiles/".$value->getRelativePathname();
        }

        return $all_images;
    }

    public function save_tile($data)
    {
        $saved_tile = $this->students->save_tile($data);
        return $saved_tile;
    }

    public function logtile($data)
    {
        $saved_tile = $this->students->logtile($data);
        return $this->absorb($saved_tile);
    }

    public function getPecsImages($id)
    {
        dump($id);

        $images = $this->students->gitMedia($id);
    }

    public function getStudentPecs($id)
    {
        $student_pecs = $this->students->getPecsByStudentID($id);
        return $this->absorb($student_pecs);
    }

    public function deleteTile($tileid)
    {
        $student_pecs = $this->students->deleteTile($tileid);
    }

    public function get_top_tiles($id)
    {
        $top_tiles = $this->students->get_top_tiles($id);
        return $top_tiles;
    }

    public function tileStats($id)
    {
        $stats = $this->students->tileStats($id);
        return $stats;
    }

    public function loginStudent($data)
    {
        $login_info = $this->students->loginStudent($data);
        return $this->absorb($login_info);
    }

    public function getStudentPecsImages($id)
    {
        $login_info = $this->students->studentPecsImages($id);
        return $this->absorb($login_info);
    }

    public function updateStudentInfo($data)
    {
        $login_info = $this->students->updateStudentInfo($data);
        return $this->absorb($login_info);
    }

    public function getStudentInfo($id)
    {
        $login_info = $this->students->getStudentInfo($id);
        return $this->absorb($login_info);
    }

    public function get_sentenses($id)
    {
        $stats = $this->students->getSentenses($id);
        return $stats;
    }

    public function get_notes($id)
    {
        $stats = $this->students->get_notes($id);
        return $stats;
    }

    public function save_student_information($id, $text)
    {
        $stats = $this->students->save_sentense_per_student($id, $text);
        return $stats;
    }

    public function add_student_notes($data)
    {
        $stats = $this->students->add_student_notes($data);
        return $stats;
    }
}
