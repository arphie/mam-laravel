<?php

namespace App\Http\Services\teachers;

use App\Http\Services\BaseService;
use Session;

// list of repository
use App\Http\Repositories\TeachersRepository;
use App\Http\Repositories\StudentsRepository;

use App\Exports\SentenseExport;
use Maatwebsite\Excel\Facades\Excel;

class TeacherService extends BaseService
{   
    private $teacher;
    // private $model;

    public function __construct(
        TeachersRepository $teachersRepo,
        StudentsRepository $studentRepo
    ){
        $this->teacher = $teachersRepo;
        $this->student = $studentRepo;
        // $this->model = 'LandingPagesModel';
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register($data)
    {   
        // $data['model'] = $this->model;
        $social = $this->teacher->register($data);
        $hasError = '';
        if($social == 0){
            $hasError = 'Something went wrong. please choose another email address';
            $teacher_id = Session::get('user');
            $profile = $this->getTeacherProfile($teacher_id);
            return view('add_teacher')->with(['profile' => $profile, 'active_user' => $profile, 'error' => $hasError]);
        } else {
            return redirect('/');
        }

        // return $this->absorb($social);

        // return view('add_teacher', ["user_id"=> $social]);
        
        // return redirect('/teacher/add')->with(["user_id"=> $social]);

    }

    public function update($data)
    {
        // $data['model'] = $this->model;
        $social = $this->teacher->update($data);
        return $this->absorb($social);
    }

    public function delete($id)
    {
        $data = [];
        $data['id'] = $id;
        // $data['model'] = $this->model;
        $social = $this->teacher->delete($data);
        return $this->absorb($social);
    }

    public function saveTeacher($data)
    {
        $teacherinfo = $this->teacher->saveTeacherAccount($data);
        return $this->absorb($teacherinfo);
    }

    public function insertTeacherInfo($data)
    {
        dump($data);
        exit;
    }

    public function get_teacher_details($student_id)
    {
        $teacher_info = $this->teacher->getTeacherDetails($student_id);
        return $teacher_info;
    }

    public function sendTeacherRequest($data)
    {
        $requestinfo = $this->teacher->sendStudentRequest($data);
        return $this->absorb($requestinfo);
    }

    public function getTeacherProfile($id)
    {
        $requestinfo = $this->teacher->getTeacherProfile($id);
        return $requestinfo;
    }

    public function getPairedStudents($id, $type)
    {
        $paired_students = $this->teacher->getPairedStudents($id, $type);
        return $paired_students;
    }

    public function getAllTeachers()
    {
        $all_teachers = $this->teacher->getAllTeachers();
        return $all_teachers;
    }

    public function loginTeacher($data)
    {
        $all_teachers = $this->teacher->loginTeacher($data);
        return $all_teachers;
        // return $this->absorb($all_teachers);
    }

    public function updateTeacherProfile($data)
    {
        $all_teachers = $this->teacher->updateTeacherProfile($data);
        return $all_teachers;
    }

    public function getTeacherInfo($data)
    {
        $teacher_info = $this->teacher->getTeacherInfo($data);
        return $this->absorb($teacher_info);
    }

    public function testmail()
    {
        $this->teacher->testmail();
    }

    public function forgotPass($data)
    {
        $teacher_info = $this->teacher->forgotPass($data);
        return $teacher_info;
    }

    public function export_data($id, $type)
    {
        // dump('show export '.$type.' for '.$id);

        // get sentenses
        if($type == 'sentenses'){
            // $download = $this->student->export_student_sentenses($id);
            return Excel::download(new SentenseExport($id), 'sentenses.xlsx');
        }
    }
    
}
