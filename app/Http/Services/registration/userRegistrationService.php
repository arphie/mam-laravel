<?php

namespace App\Http\Services\registration;

use App\Http\Services\BaseService;

// list of repository
use App\Http\Repositories\StudentsRepository;

class userRegistrationService extends BaseService
{   
    private $student;
    // private $model;

    public function __construct(
        StudentsRepository $studentRepo
    ){
        $this->student = $studentRepo;
        // $this->model = 'LandingPagesModel';
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register($data)
    {   
        $save_student = $this->student->register($data);
        return $this->absorb($save_student);
    }

    // public function update($data)
    // {
    //     // $data['model'] = $this->model;
    //     $social = $this->teacher->update($data);
    //     return $this->absorb($social);
    // }

    // public function delete($id)
    // {
    //     $data = [];
    //     $data['id'] = $id;
    //     // $data['model'] = $this->model;
    //     $social = $this->teacher->delete($data);
    //     return $this->absorb($social);
    // }
}
