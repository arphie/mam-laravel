<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;


class CorrectSentenseModel extends BaseModel
{
    use HasFactory, Notifiable;
    // use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    public $timestamps = true;
    public $incrementing = true;
    protected $table = 'correct_sentense';

    public $casts = [
        'id' => 'int'
    ];

    protected $fillable = [
        'sentense',
        'approved_by'
    ];


    public $hidden = [];

    public $rules = [
        'sentense' => 'sometimes|required',
        'approved_by' => 'sometimes|required'
    ];

    public function transactions()
     {
         return $this->morphMany();
     }


    // /**
    //  * The attributes that should be cast to native types.
    //  *
    //  * @var array
    //  */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];
}
