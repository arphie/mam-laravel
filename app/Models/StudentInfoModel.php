<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;


class StudentInfoModel extends BaseModel
{
    use HasFactory, Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    public $timestamps = true;
    public $incrementing = true;
    protected $table = 'students';

    public $casts = [
        'id' => 'int'
    ];

    protected $fillable = [
        'first_name',
        'last_name',
        'middle_name',
        'email',
        'birth_date',
        'address',
        'school',
        'contact_number',
        'password',
    ];


    public $hidden = [];

    public $rules = [
        'first_name' => 'sometimes|required',
        'last_name' => 'sometimes|required',
        'email' => 'sometimes|required',
        'birth_date' => 'sometimes|required',
        'address' => 'sometimes|required',
        'school' => 'sometimes|required',
        'contact_number' => 'sometimes|required'
    ];

    public function transactions()
     {
         return $this->morphMany();
     }


    // /**
    //  * The attributes that should be cast to native types.
    //  *
    //  * @var array
    //  */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];
}
