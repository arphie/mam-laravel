<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;


class PairedStudentsModel extends BaseModel
{
    use HasFactory, Notifiable;
    // use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    public $timestamps = true;
    public $incrementing = true;
    protected $table = 'paired_students';

    public $casts = [
        'id' => 'int'
    ];

    protected $fillable = [
        'user_id',
        'teacher_id',
        'status'
    ];


    public $hidden = [];

    public $rules = [
        'user_id' => 'sometimes|required',
        'teacher_id' => 'sometimes|required',
        'status' => 'sometimes|required'
    ];

    public function transactions()
     {
         return $this->morphMany();
     }


    // /**
    //  * The attributes that should be cast to native types.
    //  *
    //  * @var array
    //  */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];
}
