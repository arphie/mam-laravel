<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use App\Models\PecsLogModel;
use Maatwebsite\Excel\Concerns\FromArray;


class SentenseExport implements FromArray
{

    private $type;
    private $student_id;

    public function __construct($userid){
        $this->student_id = $userid;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {
        // $student_info = $this->returnToArray(PecsLogModel::where('student_id', '=', $this->student_id)->get());

        $student_info = PecsLogModel::where('student_id', '=', $this->student_id)->get();
        
        $student_info = ($student_info !== NULL ? $student_info->toArray() : []);
        
        // get list of sentenses
        $list_of_sentense = [];
        foreach ($student_info as $key => $value) {
            $list_of_sentense[$value['septrace']]['sentense'] = $value['sentense'];
            $list_of_sentense[$value['septrace']]['created_at'] = Carbon::parse($value['created_at'])->format('M d,Y g:iA');
        }

        // sanitize data
        $to_export = [];
        foreach ($list_of_sentense as $senskey => $sensvalue) {
            $export_arrangement = [];
            $export_arrangement[0] = $sensvalue['sentense'];
            $export_arrangement[1] = $sensvalue['created_at'];
            array_push($to_export, $export_arrangement);
        }
        // return new Collection($to_export);
        return $to_export;
        // return PecsLogModel::all();
    }
}
