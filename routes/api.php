<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// register api
Route::post("insert", 'registrationController@injectUser');


// students
Route::post("student/insert", 'studentsController@saveStudent');
Route::get("student/{id}/approve", 'studentsController@approveStudent');

// teacher
Route::post("teacher/insert", 'teacherController@saveTeacher');

Route::post("teacher/info/insert", 'teacherController@insertTeacherInfo');

// connectivity
Route::post("teacher/request", 'teacherController@studentRequest');

// register api
Route::post("/login", 'authController@processloginapi');

Route::post("/student/tracking", 'studentsController@trackuseractivity');

Route::post("/student/info/update", 'studentsController@updateStudentInfo');

Route::get("/student/info/{id}", 'studentsController@getStudentInfo');

// get info 
Route::get("/pecs/download", 'studentsController@getPecsImages');

Route::get('/teacher/info', 'teacherController@get_teacher_info');

Route::get('/student/pecs/{id}', 'studentsController@get_student_pecs');

Route::get('/student/pecs/entry', 'studentsController@get_student_pecs');

Route::get('/student/pecs/image/{id}', 'studentsController@get_student_pecs_images');

Route::post("/mail", 'teacherController@testmail');
