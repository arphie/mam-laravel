<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('dashboard');
    return redirect('/login'); 
});

Route::get('/405', 'authController@userNotAllowed');

Route::group([
    'middleware' => 'isloggedin',
    'prefix' => 'students',
], function () {
    Route::group([
        'prefix' => 'profile',
    ], function () {
        Route::get('/{id}', 'studentsController@profile');
        Route::get('/{id}/cs/{text}', 'studentsController@saveCorrectSentense');
        Route::get('/tiles/{id}/add', 'studentsController@introduce_tile');
        Route::post('/tiles/{id}/add', 'studentsController@process_introduce_tile');
        Route::post('/add/notes', 'studentsController@add_student_notes');
        Route::get('/{id}/delete/{tileid}', 'studentsController@deletetile');
    });
    Route::get('/', 'studentsController@view_page');
    Route::get('/request', 'studentsController@students_request');
});


Route::group([
    'middleware' => 'isloggedin',
    'prefix' => 'teacher',
], function () {
    Route::group([
        'prefix' => 'profile',
    ], function () {
        Route::get('/{id}', 'teacherController@profile');
        Route::get('/changepass', 'teacherController@change_password');
        Route::post('/update', 'teacherController@updateprofile');
    });
    
    Route::get('add', 'teacherController@add_teacher');

    Route::post('add', 'teacherController@process_add_teacher');
    
});


Route::get('/login', 'authController@login');
Route::post('/login', 'authController@processlogin');

Route::get('/logout', 'authController@logout');

// View registration page :: Guest
Route::get('/register', 'authController@register');

Route::get('/download', 'authController@download');

Route::get('/download/apk', 'authController@downloadapk');
Route::get('/download/ios', 'authController@downloadios');

Route::get('/export/{id}/{type}', 'teacherController@export');

// Process registration :: Guest
Route::post('/register', 'authController@process_registration');

Route::post('/forgotPass', 'authController@forgotPass');
